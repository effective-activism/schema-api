<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SchemaApi\Security\ExampleSchemaApiVoter;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatement;
use EffectiveActivism\SparQlClient\Syntax\Statement\SelectStatement;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\ZeroOrMorePath;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\InMemoryUser;
use LogicException;

class ExampleSchemaApiVoterTest extends WebTestCase
{
    public function testSupports()
    {
        $placeholderTriple = new Triple(new Variable('a'), new Variable('b'), new Variable('c'));
        $exampleVoter = new ExampleSchemaApiVoter();
        $this->assertEquals(-1, $exampleVoter->vote($this->createMock(TokenInterface::class), new SelectStatement([]), [Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR]));
        $this->assertEquals(-1, $exampleVoter->vote($this->createMock(TokenInterface::class), new InsertStatement([$placeholderTriple]), [Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT]));
        $this->assertEquals(-1, $exampleVoter->vote($this->createMock(TokenInterface::class), new SelectStatement([]), [Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE]));
        $this->assertEquals(-1, $exampleVoter->vote($this->createMock(TokenInterface::class), new SelectStatement([]), [Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE]));
        $statement = new SelectStatement([], [
            'schema' => 'https://schema.org/',
        ]);
        $statement->where([
            new Triple(new Iri('urn:uuid:878f0d06-ae71-11eb-8c52-37bf6fc4ed32'), new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), new PrefixedIri('schema', 'Role')),
        ]);
        $this->assertEquals(1, $exampleVoter->vote($this->createMock(TokenInterface::class), $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE]));
        $this->assertEquals(1, $exampleVoter->vote($this->createMock(TokenInterface::class), $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE]));
        $tokenMock = $this->createMock(TokenInterface::class);
        $tokenMock->method('getUser')->willReturn(new InMemoryUser('lorem', 'ipsum'));
        $this->assertEquals(1, $exampleVoter->vote($tokenMock, $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT]));
        $this->assertEquals(1, $exampleVoter->vote($tokenMock, $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE]));
        $this->assertEquals(1, $exampleVoter->vote($tokenMock, $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE]));
        $exampleVoterWithPublicVoteOnAttribute = new class () extends ExampleSchemaApiVoter {
            public function publicVoteOnAttribute(string $attribute, $subject, TokenInterface $token) {
                return $this->voteOnAttribute($attribute, $subject, $token);
            }
        };
        // Test for unreachable statement.
        $exceptionThrown = false;
        try {
            $exampleVoterWithPublicVoteOnAttribute->publicVoteOnAttribute('lorem', $statement, $this->createMock(TokenInterface::class));
        } catch (LogicException) {
            $exceptionThrown = true;
        }
        $this->assertTrue($exceptionThrown);
        // Test insertion of WebPage.
        $statement = new InsertStatement([$placeholderTriple], [
            'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            'schema' => 'https://schema.org/',
        ]);
        $statement->where([
            new Triple(new Iri('urn:uuid:878f0d06-ae71-11eb-8c52-37bf6fc4ed32'), new PrefixedIri('rdf', 'type'), new PrefixedIri('schema', 'WebPage')),
        ]);
        $this->assertEquals(1, $exampleVoter->vote($this->createMock(TokenInterface::class), $statement, [Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT]));
    }
}
