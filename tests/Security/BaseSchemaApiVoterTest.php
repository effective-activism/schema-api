<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SchemaApi\Security\BaseSchemaApiVoter;
use EffectiveActivism\SparQlClient\Syntax\Statement\SelectStatement;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class BaseSchemaApiVoterTest extends WebTestCase
{
    public function testSupports()
    {
        $class = new class() extends BaseSchemaApiVoter implements VoterInterface {

            protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
            {
                return false;
            }

            public function publicSupports(string $attribute, $subject) {
                return $this->supports($attribute, $subject);
            }
        };
        $this->assertFalse($class->publicSupports('lorem', null));
        $this->assertFalse($class->publicSupports(Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR, null));
        $this->assertTrue($class->publicSupports(Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR, new SelectStatement([])));
    }
}