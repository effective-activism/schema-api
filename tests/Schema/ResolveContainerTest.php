<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ResolveContainerTest extends KernelTestCase
{
    public function testResolveContainer()
    {
        $resolveContainer = new ResolveContainer();
        $iri1 = new Iri('https://schema.org/headline');
        $resolveContainer->setIncomingProperty($iri1);
        $this->assertEquals($iri1, $resolveContainer->getIncomingProperty());
        $iri2 = new Iri('https://schema.org/identifier');
        $resolveContainer->setProperty($iri2);
        $this->assertEquals($iri2, $resolveContainer->getProperty());
        $this->assertFalse($resolveContainer->hasValue());
        $iri3 = new Iri('https://schema.org/specialty');
        $resolveContainer->setValue($iri3);
        $this->assertEquals($iri3, $resolveContainer->getValue());
        $this->assertTrue($resolveContainer->hasValue());
        $filters = [new Triple($iri1, $iri2, $iri3)];
        $resolveContainer->setFilters($filters);
        $this->assertEquals($filters, $resolveContainer->getFilters());
        $this->expectException(SchemaApiException::class);
        $resolveContainer->setFilters([$iri1]);
    }
}
