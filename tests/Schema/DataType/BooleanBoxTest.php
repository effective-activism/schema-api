<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\DataType\BooleanBox;
use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BooleanBoxTest extends KernelTestCase
{
    const NAME = 'BooleanBox';

    public function testDataType()
    {
        $box = new BooleanBox();
        $this->assertEquals(self::NAME, $box->config['name']);
        $closure = $box->config['fields']['value']['resolve'];
        $resolveContainer = new ResolveContainer();
        $this->assertNull($closure($resolveContainer, []));
        $resolveContainer->setValue(new PlainLiteral(true));
        $this->assertTrue($closure($resolveContainer, []));
        $resolveContainer->setValue(new PlainLiteral(false));
        $this->assertFalse($closure($resolveContainer, []));
        $resolveContainer->setValue(new PlainLiteral('lorem'));
        $this->expectException(SchemaApiException::class);
        $closure($resolveContainer, []);
    }
}
