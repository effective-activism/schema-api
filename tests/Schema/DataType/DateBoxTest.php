<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\DataType\DateBox;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateBoxTest extends KernelTestCase
{
    const NAME = 'DateBox';

    public function testDataType()
    {
        $box = new DateBox();
        $this->assertEquals(self::NAME, $box->config['name']);
        $closure = $box->config['fields']['value']['resolve'];
        $resolveContainer = new ResolveContainer($this->createMock(SparQlClientInterface::class));
        $resolveContainer->setValue(new PlainLiteral('lorem'));
        $this->assertEquals('lorem', $closure($resolveContainer, []));
    }
}
