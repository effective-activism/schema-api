<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\DataType\FloatBox;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FloatBoxTest extends KernelTestCase
{
    const NAME = 'FloatBox';

    public function testDataType()
    {
        $box = new FloatBox();
        $this->assertEquals(self::NAME, $box->config['name']);
        $closure = $box->config['fields']['value']['resolve'];
        $resolveContainer = new ResolveContainer($this->createMock(SparQlClientInterface::class));
        $resolveContainer->setValue(new PlainLiteral(12.4));
        $this->assertEquals(12.4, $closure($resolveContainer, []));
    }
}
