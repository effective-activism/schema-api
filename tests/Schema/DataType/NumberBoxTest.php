<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\DataType\NumberBox;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NumberBoxTest extends KernelTestCase
{
    const NAME = 'NumberBox';

    public function testDataType()
    {
        $box = new NumberBox();
        $this->assertEquals(self::NAME, $box->config['name']);
        $closure = $box->config['fields']['value']['resolve'];
        $resolveContainer = new ResolveContainer($this->createMock(SparQlClientInterface::class));
        $resolveContainer->setValue(new PlainLiteral(12));
        $this->assertEquals(12, $closure($resolveContainer, []));
    }
}
