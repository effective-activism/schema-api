<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\DataType\TimeBox;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TimeBoxTest extends KernelTestCase
{
    const NAME = 'TimeBox';

    public function testDataType()
    {
        $box = new TimeBox();
        $this->assertEquals(self::NAME, $box->config['name']);
        $closure = $box->config['fields']['value']['resolve'];
        $resolveContainer = new ResolveContainer($this->createMock(SparQlClientInterface::class));
        $resolveContainer->setValue(new PlainLiteral('11:00'));
        $this->assertEquals('11:00', $closure($resolveContainer, []));
    }
}
