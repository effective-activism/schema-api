<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\Exception;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SchemaApiExceptionTest extends KernelTestCase
{
    public function testException()
    {
        $exception = new SchemaApiException();
        $this->assertTrue($exception->isClientSafe());
        $this->assertEquals('businessLogic', $exception->getCategory());
    }
}
