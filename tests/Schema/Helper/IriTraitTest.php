<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema\Helper;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\Helper\IriTrait;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IriTraitTest extends KernelTestCase
{
    public function testIriTrait()
    {
        $class = new class {
            use IriTrait;

            protected array $iris = [];

            protected array $namespaces = [];

            public function __construct()
            {
                $this->namespaces = [
                    'schema' => 'https://schema.org/',
                ];
                $this->iris = [
                    'headline' => 'https://schema.org/headline',
                ];
            }
        };
        $iri = 'https://schema.org/headline';
        $this->assertEquals('headline', $class->getLocalPart($iri));
        $thrownException = false;
        try {
            $class->getLocalPart('lorem');
        } catch (SchemaApiException) {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $this->assertEquals('https://schema.org/headline', $class->getIri('headline'));
        $thrownException = false;
        try {
            $class->getIri('lorem');
        } catch (SchemaApiException) {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $iri = 'https://schema.org/headline';
        $prefixedIri = $class->attemptCreatePrefixedIri($iri);
        $this->assertInstanceOf(PrefixedIri::class, $prefixedIri);
        $iri = 'https://unknown.org/headline';
        $failedPrefixedIri = $class->attemptCreatePrefixedIri($iri);
        $this->assertInstanceOf(Iri::class, $failedPrefixedIri);
        $iri = 'https://schema.org/headline';
        $prefixedIri = $class->attemptConvertToPrefixedIri(new Iri($iri));
        $this->assertInstanceOf(PrefixedIri::class, $prefixedIri);
        $thrownException = false;
        $prefixedIri = new PrefixedIri('schema', 'headline');
        $iri = $class->createIriFromPrefixedIri($prefixedIri);
        $this->assertInstanceOf(Iri::class, $iri);
        try {
            $class->createIriFromPrefixedIri(new PrefixedIri('unknown', 'headline'));
        } catch (SchemaApiException) {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }
}
