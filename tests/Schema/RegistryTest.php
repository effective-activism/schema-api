<?php

namespace EffectiveActivism\SchemaApi\Tests\Schema;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SchemaApi\Schema\DataType\BooleanBox;
use EffectiveActivism\SchemaApi\Schema\DataType\DateBox;
use EffectiveActivism\SchemaApi\Schema\DataType\DateTimeBox;
use EffectiveActivism\SchemaApi\Schema\DataType\FloatBox;
use EffectiveActivism\SchemaApi\Schema\DataType\IntegerBox;
use EffectiveActivism\SchemaApi\Schema\DataType\NumberBox;
use EffectiveActivism\SchemaApi\Schema\DataType\TextBox;
use EffectiveActivism\SchemaApi\Schema\DataType\TimeBox;
use EffectiveActivism\SchemaApi\Schema\DataType\UrlBox;
use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\Registry;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use EffectiveActivism\SchemaApi\Validation\NoValidation;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Exception\SparQlException;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Filter;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\TripleInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\TypedLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use GraphQL\Error\InvariantViolation;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RegistryTest extends KernelTestCase
{
    public function testGetType()
    {
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $this->createMock(SparQlClientInterface::class), new NoValidation());
        $this->assertInstanceOf(BooleanBox::class, $registry->getType('https://schema.org/Boolean'));
        $this->assertInstanceOf(DateBox::class, $registry->getType('https://schema.org/Date'));
        $this->assertInstanceOf(DateTimeBox::class, $registry->getType('https://schema.org/DateTime'));
        $this->assertInstanceOf(FloatBox::class, $registry->getType('https://schema.org/Float'));
        $this->assertInstanceOf(IntegerBox::class, $registry->getType('https://schema.org/Integer'));
        $this->assertInstanceOf(NumberBox::class, $registry->getType('https://schema.org/Number'));
        $this->assertInstanceOf(TextBox::class, $registry->getType('https://schema.org/Text'));
        $this->assertInstanceOf(TimeBox::class, $registry->getType('https://schema.org/Time'));
        $this->assertInstanceOf(UrlBox::class, $registry->getType('https://schema.org/URL'));
    }

    public function testGetMutation()
    {
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([
            [
                'class' => new Iri('https://schema.org/WebPage'),
                'type' => new Iri('https://schema.org/WebPage'),
                'property' => new Iri('https://schema.org/headline'),
                'comment' => new PlainLiteral('Lorem Ipsum'),
            ],
        ]);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $mutationObject = $registry->getMutation();
        $closure = $mutationObject->config['fields'];
        $fields = $closure();
        $this->assertIsArray($fields);
        $insertWebPageClosure = $fields['insertWebPage']['resolve'];
        $thrownException = false;
        try {
            $insertWebPageClosure(null, [
                'values' => [
                    'headline' => 'Lorem',
                ],
            ], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $webPageClosure = $fields['updateWebPage']['resolve'];
        $thrownException = false;
        try {
            $webPageClosure(null, [
                'filters' => [
                    'headline' => 'Lorem',
                ],
            ], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $deleteWebPageClosure = $fields['deleteWebPage']['resolve'];
        $thrownException = false;
        try {
            $deleteWebPageClosure(null, [
                'filters' => [
                    'headline' => 'Lorem',
                ],
            ], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }

    public function testGetQuery()
    {
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([
            [
                'class' => new Iri('https://schema.org/WebPage'),
                'type' => new Iri('https://schema.org/WebPage'),
                'property' => new Iri('https://schema.org/headline'),
                'comment' => new PlainLiteral('Lorem Ipsum'),
            ],
        ]);
        $property = new ReflectionProperty(Registry::class, 'iris');
        $property->setAccessible(true);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], true, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $queryObject = $registry->getQuery();
        $closure = $queryObject->config['fields'];
        $fields = $closure();
        $this->assertIsArray($fields);
        $webPageClosure = $fields['getWebPage']['resolve'];
        $thrownException = false;
        try {
            $webPageClosure(null, [
                'filters' => [
                    'headline' => [
                        'equalTo' => 'Lorem'
                    ],
                ],
            ], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $webPagesClosure = $fields['getWebPages']['resolve'];
        $thrownException = false;
        try {
            $webPagesClosure(null, [
                'filters' => [
                    'headline' => [
                        'equalTo' => 'Lorem'
                    ],
                ],
            ], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);

        $objectMock = $this->createMock(ObjectType::class);
        $objectMock->method('getField')->willThrowException(new InvariantViolation());
        $objectMock->name = 'WebPage';
        $listOfMock = $this->createMock(ListOfType::class);
        $listOfMock->method('getOfType')->willReturn($objectMock);
        $resolveInfoMock = $this->createMock(ResolveInfo::class);
        $resolveInfoMock->returnType = $listOfMock;
        $registryIris = $property->getValue($registry);
        $registryIris['headline'] = 'https://schema.org/headline';
        $property->setValue($registry, $registryIris);

        $webPagesClosure = $fields['getWebPages']['resolve'];
        $thrownException = false;
        try {
            $webPagesClosure(null, [
                'filters' => [
                    'headline' => [
                        'equalTo' => 'urn:uuid:4a8df056-9559-11eb-a74b-bb906d84112b',
                        'type' => 'HeadlineRole',
                    ],
                ],
                'orderBy' => 'invalidOrderBy',
                'limit' => 0,
                'offset' => 0,
            ], null, $resolveInfoMock);
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }

    public function testGetUnionException()
    {
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $this->createMock(SparQlClientInterface::class), new NoValidation());
        $thrownException = false;
        try {
            $registry->getUnion('lorem');
        } catch (SchemaApiException $exception) {
            $thrownException = true;
            $this->assertEquals(sprintf('"%s" is not found in union registry', 'lorem'), $exception->getMessage());
        }
        $this->assertTrue($thrownException);
    }

    public function testBuildArguments()
    {
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([
            [
                'class' => new Iri('https://schema.org/WebPage'),
                'type' => new Iri('https://schema.org/WebPage'),
                'property' => new Iri('https://schema.org/headline'),
                'comment' => new PlainLiteral('Lorem Ipsum'),
            ],
        ]);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $method = new ReflectionMethod($registry, 'buildArguments');
        $method->setAccessible(true);
        $argumentType = $method->invoke($registry, 'https://schema.org/WebPage');
        $this->assertInstanceOf(InputObjectType::class, $argumentType);
        $closure = $argumentType->config['fields'];
        $this->assertIsArray($closure());
    }

    public function testBuildClass()
    {
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([
            [
                'class' => new Iri('https://schema.org/WebPage'),
                'type' => new Iri('https://schema.org/WebPage'),
                'property' => new Iri('https://schema.org/headline'),
                'comment' => new PlainLiteral('Lorem Ipsum'),
            ],
        ]);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $type = $registry->getType('https://schema.org/headline');
        $this->assertEquals('[UnionOfHeadline]', $type->toString());
        $method = new ReflectionMethod($registry, 'buildClass');
        $method->setAccessible(true);
        $objectType = $method->invoke($registry, 'https://schema.org/WebPage');
        $this->assertInstanceOf(ObjectType::class, $objectType);
        $closure = $objectType->config['fields'];
        $fields = $closure();
        $this->assertIsArray($fields);

        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([
            [
                'class' => new Iri('https://schema.org/_foo'),
                'property' => new Iri('https://schema.org/_foo'),
                'comment' => new PlainLiteral('Lorem Ipsum'),
            ],
        ]);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $method = new ReflectionMethod($registry, 'buildClass');
        $method->setAccessible(true);
        $objectType = $method->invoke($registry, 'https://schema.org/_foo');
        $this->assertInstanceOf(ObjectType::class, $objectType);
        $closure = $objectType->config['fields'];
        $this->expectException(SchemaApiException::class);
        $closure();
    }

    public function testBuildProperty()
    {
        $iriProperty = new ReflectionProperty(Registry::class, 'iris');
        $iriProperty->setAccessible(true);
        $descriptionProperty = new ReflectionProperty(Registry::class, 'descriptions');
        $descriptionProperty->setAccessible(true);
        $authorizationCheckerMock = $this->createMock(AuthorizationCheckerInterface::class);
        $authorizationCheckerMock->method('isGranted')->willReturn(true);
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $authorizationCheckerMock, $sparQlClientMock, new NoValidation());
        $type = $registry->getType('https://schema.org/headline');
        $this->assertEquals('[UnionOfHeadline]', $type->toString());
        $method = new ReflectionMethod($registry, 'buildProperty');
        $method->setAccessible(true);
        $unionType = $method->invoke($registry, 'https://schema.org/headline');
        $this->assertInstanceOf(UnionType::class, $unionType);
        $closure = $unionType->config['resolveType'];
        $valueTerm = new PlainLiteral('lorem');
        $valueTerm->setVariableName('isUnknown');
        $resolveContainer = new ResolveContainer();
        $resolveContainer
            ->setFilters([
                new Triple(new Variable('subject'), new PrefixedIri('schema', 'headline'), new PlainLiteral('Lorem'))
            ])
            ->setValue($valueTerm);
        $thrownException = false;
        try {
            $closure($resolveContainer);
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        // Test class resolve with match.
        $valueTerm = new Iri('https://schema.org/mainContentOfPage');
        $valueTerm->setVariableName('isClass');
        $sparQlClientMock->method('execute')->willReturn([
            [
                'classType' => new Iri('https://schema.org/WebPageElement'),
            ]
        ], []);
        $registryIris = $iriProperty->getValue($registry);
        $registryIris['WebPageElement'] = 'https://schema.org/WebPageElement';
        $iriProperty->setValue($registry, $registryIris);
        $registryDescriptions = $descriptionProperty->getValue($registry);
        $registryDescriptions['WebPageElement'] = 'foo';
        $descriptionProperty->setValue($registry, $registryDescriptions);
        $resolveContainer = new ResolveContainer();
        $resolveContainer
            ->setFilters([
                new Triple(new Variable('subject'), new PrefixedIri('schema', 'mainContentOfPage'), new Iri('urn:uuid:23b541e4-d4ee-11eb-9a5d-5b506c2cd9a2'))
            ])
            ->setValue($valueTerm);
        $this->assertInstanceOf(Type::class, $closure($resolveContainer));
        // Test class resolve when no matches.
        // The second call to $sparQlClientMock->execute() will return an empty array,
        // which means there are no content matches to the urn (and thus no type match).
        $resolveContainer = new ResolveContainer();
        $resolveContainer
            ->setFilters([
                new Triple(new Variable('subject'), new PrefixedIri('schema', 'mainContentOfPage'), new Iri('urn:uuid:23b541e4-d4ee-11eb-9a5d-5b506c2cd9a2'))
            ])
            ->setValue($valueTerm);
        $this->expectException(SchemaApiException::class);
        $closure($resolveContainer);
    }

    public function testBuildRoleClass()
    {
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $this->createMock(SparQlClientInterface::class), new NoValidation());
        $type = $registry->getType('https://schema.org/headline');
        $this->assertEquals('[UnionOfHeadline]', $type->toString());
        $method = new ReflectionMethod($registry, 'buildRoleClass');
        $method->setAccessible(true);
        $thrownException = false;
        $roleType = $method->invoke($registry, 'https://schema.org/headline');
        $closure = $roleType->config['fields'];
        $fields = $closure();
        $field = array_pop($fields);
        $fieldClosure = $field['resolve'];
        $valueTerm = new Iri('https://schema.org/headline');
        $resolveContainer = new ResolveContainer();
        $resolveContainer
            ->setFilters([
                new Triple(new Variable('subject'), new PrefixedIri('schema', 'headline'), new PlainLiteral('Lorem'))
            ])
            ->setValue($valueTerm);
        try {
            $fieldClosure($resolveContainer, [], null, $this->createMock(ResolveInfo::class));
        } catch (SchemaApiException $exception) {
            $this->assertEquals('Operation not allowed', $exception->getMessage());
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }

    /**
     * @covers \EffectiveActivism\SchemaApi\Schema\Registry::determineLiteralType
     */
    public function testDetermineLiteralType()
    {
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([], [], true, true);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $method = new ReflectionMethod($registry, 'determineLiteralType');
        $method->setAccessible(true);

        $value = new TypedLiteral('true', new PrefixedIri('xsd', 'boolean'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'BooleanBox');

        $value = new TypedLiteral('2021-01-01+01:00', new PrefixedIri('xsd', 'date'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'DateBox');

        $value = new TypedLiteral('2021-01-01T12:02:22.000+01:00', new PrefixedIri('xsd', 'dateTime'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'DateTimeBox');

        $value = new TypedLiteral('12.3', new PrefixedIri('xsd', 'decimal'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'NumberBox');

        $value = new TypedLiteral('11', new PrefixedIri('xsd', 'integer'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'IntegerBox');

        $value = new TypedLiteral('12:02:22+01:00', new PrefixedIri('xsd', 'time'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'TimeBox');

        $value = new TypedLiteral('Lorem', new PrefixedIri('xsd', 'string'));
        $type = $method->invoke($registry, $value);
        $this->assertEquals($type->name, 'TextBox');

        $value = new TypedLiteral('Lorem', new PrefixedIri('xsd', 'unknown'));
        $this->expectException(SchemaApiException::class);
        $method->invoke($registry, $value);
    }

    public function testResolveFilterValues()
    {
        $property = new ReflectionProperty(Registry::class, 'iris');
        $property->setAccessible(true);
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([], [], true, true);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $type = $registry->getType('https://schema.org/headline');
        $this->assertEquals('[UnionOfHeadline]', $type->toString());
        $method = new ReflectionMethod($registry, 'resolveFilterValue');
        $method->setAccessible(true);
        $subjectVariable = new Variable('lorem');
        $predicate = new Iri('https://schema.org/headline');
        $term = new PlainLiteral('foo');
        $filterType = Constant::FILTER_EQUAL_TO;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s = "foo")', $variableName), $filter->serialize());

        $filterType = Constant::FILTER_GREATER_THAN;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s > "foo")', $variableName), $filter->serialize());

        $filterType = Constant::FILTER_GREATER_THAN_OR_EQUAL_TO;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s >= "foo")', $variableName), $filter->serialize());

        $filterType = Constant::FILTER_LESS_THAN;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s < "foo")', $variableName), $filter->serialize());

        $filterType = Constant::FILTER_LESS_THAN_OR_EQUAL_TO;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s <= "foo")', $variableName), $filter->serialize());

        $filterType = Constant::FILTER_NOT_EQUAL_TO;
        $filters = $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        /** @var TripleInterface $triple */
        $triple = $filters[0];
        /** @var Filter $filter */
        $filter = $filters[1];
        $variableName = $triple->getObject()->getVariableName();
        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertEquals(sprintf('FILTER(?%s != "foo")', $variableName), $filter->serialize());

        $term = new Iri('http://schema.org/foo');
        $filterType = Constant::FILTER_LESS_THAN_OR_EQUAL_TO;
        $thrownException = false;
        try {
            $method->invoke($registry, $subjectVariable, $predicate, $term, $filterType);
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }

    /**
     * @covers \EffectiveActivism\SchemaApi\Schema\Registry::resolveArgumentValues
     */
    public function testResolveArgumentValue()
    {
        $property = new ReflectionProperty(Registry::class, 'iris');
        $property->setAccessible(true);
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $sparQlClientMock->method('execute')->willReturn([], [], true, true);
        $registry = new Registry([
            'schema' => 'https://schema.org/'
        ], false, $this->createMock(AuthorizationCheckerInterface::class), $sparQlClientMock, new NoValidation());
        $type = $registry->getType('https://schema.org/headline');
        $this->assertEquals('[UnionOfHeadline]', $type->toString());
        $method = new ReflectionMethod($registry, 'resolveArgumentValue');
        $method->setAccessible(true);
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['BooleanBox'], 'Lorem', 'BooleanBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['DateBox'], '1970-01-01','DateBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['DateTimeBox'], '1970-01-01T00:00:00Z', 'DateTimeBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['FloatBox'], 'Lorem', 'FloatBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['IntegerBox'], 'Lorem', 'IntegerBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['NumberBox'], 'Lorem', 'NumberBox'));
        $this->assertInstanceOf(PlainLiteral::class, $method->invoke($registry, ['TextBox'], 'Lorem', 'TextBox'));
        $this->assertInstanceOf(TypedLiteral::class, $method->invoke($registry, ['TimeBox'], '00:00:00', 'TimeBox'));
        $this->assertInstanceOf(PrefixedIri::class, $method->invoke($registry, ['URLBox'], 'https://schema.org/headline', 'URLBox'));
        $this->assertInstanceOf(Iri::class, $method->invoke($registry, ['MemberOfRole'], 'urn:uuid:f047a3ea-c9f5-11eb-b8fb-1f8ac4ca5906', 'MemberOfRole'));
        $registryIris = $property->getValue($registry);
        $registryIris['Person'] = 'https://schema.org/Person';
        $property->setValue($registry, $registryIris);
        $this->assertInstanceOf(Iri::class, $method->invoke($registry, ['Person'], 'urn:uuid:b157335a-c9f8-11eb-ab54-1bdd9e9baac2', 'Person'));
        $thrownException = false;
        try {
            $method->invoke($registry, ['foo'], 'Lorem', 'foo');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $thrownException = false;
        try {
            $method->invoke($registry, ['foo'], 'Lorem', 'TextBox');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $thrownException = false;
        try {
            $method->invoke($registry, ['DateBox'], 'Lorem', 'DateBox');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $thrownException = false;
        try {
            $method->invoke($registry, ['DateTimeBox'], 'Lorem', 'DateTimeBox');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $thrownException = false;
        try {
            $method->invoke($registry, ['TimeBox'], 'Lorem', 'TimeBox');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
        $thrownException = false;
        try {
            $method->invoke($registry, ['TextBox'], '"""\'\'\'Lorem\'\'\'"""', 'TextBox');
        } catch (SchemaApiException)
        {
            $thrownException = true;
        }
        $this->assertTrue($thrownException);
    }
}
