<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Tests\Bundle;

use EffectiveActivism\SchemaApi\DependencyInjection\Configuration;
use EffectiveActivism\SchemaApi\DependencyInjection\SchemaApiExtension;
use EffectiveActivism\SchemaApi\EffectiveActivismSchemaApiBundle;
use EffectiveActivism\SchemaApi\Schema\Registry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class BundleTest extends KernelTestCase
{
    const TEST_CONFIG = [
        'namespaces' => [
            'schema' => 'https://schema.org/',
        ],
        'use_bds_service' => false,
    ];

    const EXPECTED_TEST_CONFIG = [
        '$namespaces' => [
            'schema' => 'https://schema.org/',
        ],
        '$useBdsService' => false,
    ];

    public function testBundle()
    {
        $bundle = new EffectiveActivismSchemaApiBundle();
        $this->assertInstanceOf(SchemaApiExtension::class, $bundle->getContainerExtension());
    }

    public function testConfiguration()
    {
        $configuration = new Configuration();
        $tree = $configuration->getConfigTreeBuilder();
        $nodes = $tree->buildTree();
        $this->assertEquals('schema_api', $nodes->getName());
    }

    public function testExtension()
    {
        $containerBuilderStub = $this->createMock(ContainerBuilder::class);
        $definition = new Definition(Registry::class, []);
        $containerBuilderStub->method('getDefinition')->willReturn($definition);
        $containerBuilderStub->method('registerForAutoconfiguration')->willReturn($this->createMock(ChildDefinition::class));
        $extension = new SchemaApiExtension();
        $extension->load([
            'schema_api' => self::TEST_CONFIG
        ], $containerBuilderStub);
        $this->assertEquals(self::EXPECTED_TEST_CONFIG, $definition->getArguments());
        $this->assertEquals('schema_api', $extension->getAlias());
    }
}
