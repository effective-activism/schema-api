<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Tests\Environment\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SecurityTest extends WebTestCase
{
    const EXPECTED_RESPONSE_SELECT_ACCESS_GRANTED = '{"data":{"getWebPage":{"headline":[{"value":"Lorem Ipsum"}]}}}';

    const EXPECTED_RESPONSE_SELECT_ACCESS_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":3}],"path":["getPerson"]}],"data":{"getPerson":null}}';

    const EXPECTED_RESPONSE_DELETE_ACCESS_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["deletePerson"]}],"data":{"deletePerson":null}}';

    const EXPECTED_RESPONSE_INSERT_ACCESS_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["insertPerson"]}],"data":{"insertPerson":null}}';

    const EXPECTED_RESPONSE_UPDATE_ACCESS_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["updatePerson"]}],"data":{"updatePerson":null}}';

    const EXPECTED_RESPONSE_TYPE_ACCESS_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":118}],"path":["getWebPage","url",0,"url"]}],"data":{"getWebPage":{"url":[{"url":null}]}}}';

    const EXPECTED_RESPONSE_MULTIPLE_QUERY_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":3}],"path":["getPersons"]}],"data":{"getPersons":null}}';

    const EXPECTED_RESPONSE_SEARCH_QUERY_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":3}],"path":["searchPersons"]}],"data":{"searchPersons":null}}';

    const EXPECTED_RESPONSE_SUBQUERY_DENIED = '{"errors":[{"message":"Operation not allowed","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":95}],"path":["getWebPage","url"]}],"data":{"getWebPage":{"url":null}}}';

    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    public function testQueryAccessGranted()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_SELECT_ACCESS_GRANTED, $response->getContent());
    }

    public function testQueryAccessDeniedWithClass()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-email-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-email-value-role-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getPerson ( filters: { identifier: { equalTo: \"4913daac-ae3f-11eb-be85-a7f8dd5a549d\" }}) { email { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_SELECT_ACCESS_DENIED, $response->getContent());
    }

    public function testQueryAccessDeniedWithRole()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-email-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-email-value-role-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getPerson ( filters: { identifier: { equalTo: \"4913daac-ae3f-11eb-be85-a7f8dd5a549d\" }}) { email { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_SELECT_ACCESS_DENIED, $response->getContent());
    }

    public function testRoleTypeDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-role-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-endDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-roleName-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-value-role-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4913daac-ae3f-11eb-be85-a7f8dd5a549d\" }}) { url { ... on UrlRole { url { ... on URLBox { value }}}}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_TYPE_ACCESS_DENIED, $response->getContent());
    }

    public function testMultipleQueryAccessDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-email-types.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getPersons ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { email { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_MULTIPLE_QUERY_DENIED, $response->getContent());
    }

    public function testSubqueryAccessDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier-2.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-value-role-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"edcda6e4-f36e-11eb-b63c-337058499d6c\" }}) { url { ... on URLBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_SUBQUERY_DENIED, $response->getContent());
    }

    public function testInsertAccessDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-email-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(''),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { insertPerson ( values: { email: { value: \"example@example\" }}) { email { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_INSERT_ACCESS_DENIED, $response->getContent());
    }

    public function testUpdateAccessDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-email-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(''),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { updatePerson ( filters: { identifier: { equalTo: \"4913daac-ae3f-11eb-be85-a7f8dd5a549d\" }} values: { email: { value: \"example@example\" }}) { email { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_UPDATE_ACCESS_DENIED, $response->getContent());
    }

    public function testDeleteAccessDenied()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-person.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-person-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-email-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { deletePerson ( filters: { identifier: { equalTo: \"4913daac-ae3f-11eb-be85-a7f8dd5a549d\" }})}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_DELETE_ACCESS_DENIED, $response->getContent());
    }
}
