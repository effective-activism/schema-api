<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Tests\Environment\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ValidationTest extends WebTestCase
{
    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTED = '{"data":{"insertWebPage":{"headline":[{"value":"Ut enim ad minim veniam"}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTION_FAILED = '{"errors":[{"message":"Validation failed. ","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["insertWebPage"]}],"data":{"insertWebPage":null}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_UPDATE_FAILED = '{"errors":[{"message":"Validation failed. ","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["updateWebPage"]}],"data":{"updateWebPage":null}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_DELETE_FAILED = '{"errors":[{"message":"Validation failed. ","extensions":{"category":"businessLogic"},"locations":[{"line":1,"column":12}],"path":["deleteWebPage"]}],"data":{"deleteWebPage":null}}';

    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    public function testEndpointInsertWebPageWithValidationRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { insertWebPage ( values: { headline: { value: \"Ut enim ad minim veniam\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTED, $response->getContent());
    }

    public function testEndpointInsertWebPageWithFailedValidationRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request-failed.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { insertWebPage ( values: { headline: { value: \"Ut enim ad minim veniam\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTION_FAILED, $response->getContent());
    }

    public function testEndpointUpdateWebPageWithFailedValidationRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request-failed.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { updateWebPage ( filters: { identifier: { equalTo: \"be45da8e-9f65-11eb-acab-cb704e349f82\" }} values: { headline: [{ value: \"Ut enim ad minim veniam\" }, { value: \"Ut enim ad minim veniam2\" }]}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_UPDATE_FAILED, $response->getContent());
    }

    public function testEndpointDeleteWebPageWithFailedValidationRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request-failed.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { deleteWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }})}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_DELETE_FAILED, $response->getContent());
    }
}
