<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Tests\Environment\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EndpointRequestTest extends WebTestCase
{
    const EXPECTED_RESPONSE_CONTENT_WEBPAGE = '{"data":{"getWebPage":{"headline":[{"value":"Lorem Ipsum"}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_DEPTH = '{"data":{"getWebPage":{"url":[{"headline":[{"value":"Lorem Ipsum"}]}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_NO_MATCH = '{"data":{"getWebPage":null}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_WITH_ROLE = '{"data":{"getWebPage":{"headline":[{"roleName":[{"value":"Ut enim ad minim veniam"}],"headline":[{"value":"dolor sit amet"}]}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGES = '{"data":{"getWebPages":[{"headline":[{"headline":[{"value":"Ut enim ad minim veniam"}]}]},{"headline":[{"value":"Lorem Ipsum"}]}]}}';

    const EXPECTED_RESPONSE_CONTENT_SEARCH_WEBPAGES = '{"data":{"getWebPages":[{"headline":[{"headline":[{"value":"Ut enim ad minim veniam"}]}]},{"headline":[{"value":"Lorem Ipsum"}]}]}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTED = '{"data":{"insertWebPage":{"headline":[{"value":"Ut enim ad minim veniam"}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_UPDATED = '{"data":{"updateWebPage":{"headline":[{"value":"Ut enim ad minim veniam"}]}}}';

    const EXPECTED_RESPONSE_CONTENT_WEBPAGE_DELETED = '{"data":{"deleteWebPage":true}}';

    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    public function testEndpointGetSchema()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-role-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-endDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-roleName-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{__schema { types { name kind description }}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(file_get_contents(__DIR__.'/../fixtures/schema.txt'), $response->getContent());
    }

    public function testEndpointGetWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE, $response->getContent());
    }

    public function testEndpointGetWebPageRequestWithClassDepth()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage-aboutpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types-with-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-aboutpage-with-parent-class-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-value-type-with-parent.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { url { ... on WebPage { headline { ... on TextBox { value }}}}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_DEPTH, $response->getContent());
    }

    public function testEndpointGetWebPageRequestNoMatch()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier-no-match.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_NO_MATCH, $response->getContent());
    }

    public function testEndpointGetWebPageWithRoleRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-role-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-endDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-roleName-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-role-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-role-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { headline { ... on HeadlineRole { roleName { ... on TextBox { value }} headline { ... on TextBox { value }}}}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_WITH_ROLE, $response->getContent());
    }

    public function testEndpointGetWebPagesRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-role-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-endDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-roleName-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpages-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-role-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPages ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }} limit: 2 offset: 1 orderBy: \"headline\" orderAscending: false ) { headline { ... on TextBox { value } ... on HeadlineRole { headline { ...on TextBox { value }}}}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGES, $response->getContent());
    }

    public function testEndpointSearchWebPagesRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-role-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-endDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-roleName-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-startDate-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-ask-true.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpages-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-role-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPages ( filters: { identifier: [{ searchFor: \"4a8df056\" } { searchFor: \"urn:uuid:4a8df056-9559-11eb-a74b-bb906d84112b\" type: \"IdentifierRole\" }]} limit: 2 offset: 1 orderBy: \"headline\" ) { headline { ... on TextBox { value } ... on HeadlineRole { headline { ...on TextBox { value }}}}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_SEARCH_WEBPAGES, $response->getContent());
    }

    public function testEndpointInsertWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { insertWebPage ( values: { headline: { value: \"Ut enim ad minim veniam\" }}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_INSERTED, $response->getContent());
    }

    public function testEndpointUpdateWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { updateWebPage ( filters: { identifier: { equalTo: \"be45da8e-9f65-11eb-acab-cb704e349f82\" }} values: { headline: [{ value: \"Ut enim ad minim veniam\" }, { value: \"Ut enim ad minim veniam2\" }]}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_UPDATED, $response->getContent());
    }

    public function testEndpointUpdateWithOverwriteWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { updateWebPage ( overwrite: true filters: { identifier: { equalTo: \"be45da8e-9f65-11eb-acab-cb704e349f82\" }} values: { headline: [{ value: \"Ut enim ad minim veniam\" }, { value: \"Ut enim ad minim veniam2\" }]}) { headline { ... on TextBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_UPDATED, $response->getContent());
    }

    public function testEndpointDeleteWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { deleteWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }})}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_DELETED, $response->getContent());
    }

    public function testEndpointDeleteWithOnlyDeleteWebPageRequest()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__ . '/../fixtures/shacl-validation-request.ntriple')),
            new MockResponse(''),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-value-roleName-type.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "mutation { deleteWebPage ( onlyDelete: { headline: { value: \"Lorem\" }} filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }})}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_CONTENT_WEBPAGE_DELETED, $response->getContent());
    }
}
