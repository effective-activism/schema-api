<?php

namespace EffectiveActivism\SchemaApi\Tests\EndpointRequest;

use EffectiveActivism\SchemaApi\Tests\Environment\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MultipleResultsOfDifferentTypesTest extends WebTestCase
{
    const EXPECTED_RESPONSE_SELECT_ACCESS_GRANTED = '{"data":{"getWebPage":{"identifier":[{"value":"Lorem Ipsum"},{"value":"http:\/\/lorem.ipsum"}]}}}';

    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    public function testMultipleResults()
    {
        $client = self::createClient();
        $httpClient = new MockHttpClient([
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-classes-webpage.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-webpage-properties.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-headline-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-url-types.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-subject-identifier.xml')),
            new MockResponse(file_get_contents(__DIR__.'/../fixtures/backend-response-get-identifier-values-types.xml')),
        ]);
        self::$kernel->getContainer()->set(HttpClientInterface::class, $httpClient);
        $cacheAdapter = new TagAwareAdapter(new ArrayAdapter());
        self::$kernel->getContainer()->set(TagAwareCacheInterface::class, $cacheAdapter);
        $client->request('POST', '/schema-api', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], '{"query": "{ getWebPage ( filters: { identifier: { equalTo: \"4a8df056-9559-11eb-a74b-bb906d84112b\" }}) { identifier { ... on TextBox { value } ... on URLBox { value }}}}"}');
        $response = $client->getInternalResponse();
        $this->assertEquals(self::EXPECTED_RESPONSE_SELECT_ACCESS_GRANTED, $response->getContent());
    }
}
