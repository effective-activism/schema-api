<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApi\Tests\Environment;

use EffectiveActivism\SchemaApi\DependencyInjection\Configuration;
use EffectiveActivism\SchemaApi\Schema\Registry;
use EffectiveActivism\SchemaApi\Validation\ValidationInterface;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TestExtension extends Extension
{
    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        if ($container->registerForAutoconfiguration(ValidationInterface::class) !== null) {
            $container->registerForAutoconfiguration(ValidationInterface::class)->addTag('schema_api.validation');
        }
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/config'));
        $loader->load('services.yml');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $definition = $container->getDefinition(Registry::class);
        $definition->setArgument('$namespaces', $config['namespaces']);
        $definition->setArgument('$useBdsService', $config['use_bds_service']);
    }

    public function getAlias(): string
    {
        return 'schema_api';
    }
}
