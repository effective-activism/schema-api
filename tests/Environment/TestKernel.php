<?php

namespace EffectiveActivism\SchemaApi\Tests\Environment;

use EffectiveActivism\SparQlClient\EffectiveActivismSparQlClientBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

class TestKernel extends Kernel
{
    public function registerBundles(): iterable
    {
        return [
            new FrameworkBundle(),
            new SecurityBundle(),
            new EffectiveActivismSparQlClientBundle(),
            new TestBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            $container->loadFromExtension('security', [
                'enable_authenticator_manager' => true,
                'firewalls' => [
                    'default' => [
                        'pattern' => '^/',
                    ]
                ],
            ]);
            $container->loadFromExtension('framework', [
                'secret' => 'test',
                'session' => [
                    'enabled' => true,
                    'storage_factory_id' => 'session.storage.factory.mock_file',
                ],
                'router' => [
                    'resource' => __DIR__ . '/config/routes.yml',
                    'utf8' => true,
                ],
                'test' => true,
            ]);
            $container->loadFromExtension('sparql_client', [
                'sparql_endpoint' => 'http://test-sparql-endpoint:9999/blazegraph/sparql',
                'shacl_endpoint' => 'http://test-shacl-endpoint:8082/blazegraph/sparql',
                'namespaces' => [
                    'schema' => 'https://schema.org/',
                    'bds' => 'http://www.bigdata.com/rdf/search#',
                ]
            ]);
            $container->loadFromExtension('schema_api', [
                'namespaces' => [
                    'schema' => 'https://schema.org/',
                ],
                'use_bds_service' => true,
            ]);
        });
    }
}
