<?php

namespace EffectiveActivism\SchemaApi\Tests\Validation;

use EffectiveActivism\SchemaApi\Validation\ExampleValidation;
use EffectiveActivism\SchemaApi\Validation\ValidationInterface;
use EffectiveActivism\SparQlClient\Client\ShaclClientInterface;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Assignment\Values;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\TripleInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\ConstructStatement;
use EffectiveActivism\SparQlClient\Syntax\Statement\ConstructStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatement;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExampleValidationTest extends KernelTestCase
{
    const NAMESPACES = [
        'schema' => 'https://schema.org/',
        'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    ];

    public function testValidationService()
    {
        $shaclClientMock = $this->createMock(ShaclClientInterface::class);
        $shaclClientMock->method('getNamespaces')->willReturn(self::NAMESPACES);
        $sparQlClientMock = $this->createMock(SparQlClientInterface::class);
        $validator = new ExampleValidation($shaclClientMock, $sparQlClientMock);
        $this->assertInstanceOf(ValidationInterface::class, $validator);
        $organizerVariable = new Variable('organizer');
        $organizerIri = new Iri('urn:uuid:65bcaf90-1f6d-11ec-be87-a3470eaf7ea2');
        $triples = [
            new Triple(
                new Iri('urn:uuid:16c0b9a4-1f6d-11ec-99d2-574372245b0b'),
                new PrefixedIri('schema', 'organizer'),
                $organizerVariable,
            ),
        ];
        $conditionals = [
            new Values([$organizerVariable], [[$organizerIri]]),
        ];
        $statement = new InsertStatement($triples, self::NAMESPACES);
        $statement->where($conditionals);
        $returnedConstructStatement = new ConstructStatement($triples, self::NAMESPACES);
        $returnedConstructStatement->where($conditionals);
        $shaclClientMock->method('convertToConstructStatement')->willReturn($returnedConstructStatement);
        /** @var ConstructStatementInterface $convertedStatement */
        $convertedStatement = $validator->convertToShaclConstructStatement($statement);
        $subject = null;
        $predicate = null;
        $object = null;
        /** @var TripleInterface $triple */
        foreach ($convertedStatement->getTriplesToConstruct() as $triple) {
            if ($triple->getSubject() === $organizerIri) {
                list($subject, $predicate, $object) = $triple->toArray();
            }
        }
        $this->assertEquals($organizerIri, $subject);
        $this->assertEquals(new PrefixedIri('rdf', 'type'), $predicate);
        $this->assertInstanceOf(Variable::class, $object);
    }
}
