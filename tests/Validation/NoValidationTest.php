<?php

namespace EffectiveActivism\SchemaApi\Tests\Validation;

use EffectiveActivism\SchemaApi\Validation\NoValidation;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatementInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NoValidationTest extends KernelTestCase
{
    public function testNoValidation()
    {
        $insertStatement = $this->createMock(InsertStatementInterface::class);
        $validator = new NoValidation();
        $this->assertTrue($validator->validate($insertStatement)->getStatus());
    }
}
