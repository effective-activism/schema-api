<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Validation;

use EffectiveActivism\SparQlClient\Syntax\Statement\ConstructStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\DeleteStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\ReplaceStatementInterface;
use EffectiveActivism\SparQlClient\Validation\ValidationResult;
use EffectiveActivism\SparQlClient\Validation\ValidationResultInterface;

class NoValidation implements ValidationInterface
{
    public function validate(ConstructStatementInterface|DeleteStatementInterface|InsertStatementInterface|ReplaceStatementInterface $statement): ValidationResultInterface
    {
        return new ValidationResult(true, []);
    }
}
