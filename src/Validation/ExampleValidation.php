<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Validation;

use EffectiveActivism\SparQlClient\Client\ShaclClientInterface;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Exception\SparQlException;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Assignment\Values;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Assignment\ValuesInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\PatternInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Statement\ConstructStatement;
use EffectiveActivism\SparQlClient\Syntax\Statement\ConstructStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\DeleteStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\ReplaceStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\TermInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use EffectiveActivism\SparQlClient\Validation\ValidationResultInterface;

class ExampleValidation implements ValidationInterface
{
    protected ShaclClientInterface $shaclClient;

    protected SparQlClientInterface $sparQlClient;

    public function __construct(ShaclClientInterface $shaclClient, SparQlClientInterface $sparQlClient)
    {
        $this->shaclClient = $shaclClient;
        $this->sparQlClient = $sparQlClient;
    }

    /**
     * @throws SparQlException
     */
    public function validate(ConstructStatementInterface|DeleteStatementInterface|InsertStatementInterface|ReplaceStatementInterface $statement): ValidationResultInterface
    {
        $convertedStatement = $this->convertToShaclConstructStatement($statement);
        return $this->shaclClient->validate($convertedStatement);
    }

    /**
     * Add explicit references to types for IRI values, to ensure that the SHACL validator can validate any sh:class.
     * @throws SparQlException
     */
    public function convertToShaclConstructStatement(ConstructStatementInterface|DeleteStatementInterface|InsertStatementInterface|ReplaceStatementInterface $statement): ConstructStatementInterface
    {
        $constructStatement = $this->shaclClient->convertToConstructStatement($statement);
        $constructTriples = $constructStatement->getTriplesToConstruct();
        $conditionalTriples = $constructStatement->getConditions();
        /** @var PatternInterface $pattern */
        foreach ($constructStatement->getConditions() as $pattern) {
            // Extract any IRIs from VALUES patterns.
            /** @var ValuesInterface $pattern */
            if (get_class($pattern) === Values::class) {
                /** @var TermInterface $term */
                foreach ($pattern->getTerms() as $term) {
                    // If an IRI is found, inform the SHACL validator about its type.
                    if (get_class($term) === Iri::class) {
                        $typeTriple = new Triple(
                            $term,
                            new PrefixedIri('rdf', 'type'),
                            new Variable(md5($term->getRawValue()))
                        );
                        $constructTriples[] = $typeTriple;
                        $conditionalTriples[] = $typeTriple;
                    }
                }
            }
        }
        $convertedConstructStatement = new ConstructStatement($constructTriples, $this->shaclClient->getNamespaces());
        $convertedConstructStatement->where($conditionalTriples);

        return $convertedConstructStatement;
    }
}
