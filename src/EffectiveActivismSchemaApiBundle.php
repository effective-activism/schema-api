<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApi;

use EffectiveActivism\SchemaApi\DependencyInjection\SchemaApiExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EffectiveActivismSchemaApiBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new SchemaApiExtension();
    }
}
