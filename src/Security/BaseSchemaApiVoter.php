<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Security;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SparQlClient\Syntax\Statement\StatementInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

abstract class BaseSchemaApiVoter extends Voter implements VoterInterface
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [
            Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE,
            Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT,
            Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE,
            Constant::SECURITY_ATTRIBUTE_QUERY_MULTIPLE,
            Constant::SECURITY_ATTRIBUTE_QUERY_SEARCH,
            Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR,
            Constant::SECURITY_ATTRIBUTE_SUBQUERY,
        ])) {
            return false;
        }
        if (
            !($subject instanceof StatementInterface) &&
            !is_array($subject)
        ) {
            return false;
        }
        return true;
    }

    abstract protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool;
}
