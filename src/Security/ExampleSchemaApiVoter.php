<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Security;

use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Optionally\OptionallyInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\PatternInterface;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\TripleInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\ConditionalStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Statement\InsertStatementInterface;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\User\InMemoryUser;


class ExampleSchemaApiVoter extends BaseSchemaApiVoter implements VoterInterface
{
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // Handle anonymous access.
        if (!$user instanceof InMemoryUser) {
            switch ($attribute) {
                // Handle access for mutations.
                case Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT:
                    /** @var InsertStatementInterface $statement */
                    $statement = $subject;
                    // If the mutation inserts a WebPage, allow it.
                    if (
                        $statement->getTriplesToInsert()[0] instanceof TripleInterface &&
                        $statement->getTriplesToInsert()[0]->getPredicate()->serialize() === 'rdf:type' &&
                        $statement->getTriplesToInsert()[0]->getObject()->serialize() === 'schema:WebPage'
                    ) {
                        return true;
                    }
                    /** @var PatternInterface $pattern */
                    foreach ($statement->getConditions() as $pattern) {
                        // If the mutation inserts a WebPage, allow it.
                        if (
                            $pattern instanceof TripleInterface &&
                            $pattern->getPredicate()->serialize() === 'rdf:type' &&
                            $pattern->getObject()->serialize() === 'schema:WebPage'
                        ) {
                            return true;
                        }
                    }
                    // If the mutation changes something other than a WebPage or Role, deny it.
                    return false;

                case Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE:
                case Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE:
                    /** @var ConditionalStatementInterface $statement */
                    $statement = $subject;
                    /** @var PatternInterface $pattern */
                    foreach ($statement->getConditions() as $pattern) {
                        // If the mutation changes a WebPage, allow it.
                        if (
                            $pattern instanceof TripleInterface &&
                            $pattern->getPredicate()->serialize() === 'rdf:type' &&
                            $pattern->getObject()->serialize() === 'schema:WebPage'
                        ) {
                            return true;
                        }
                        // Since roles are always possible, include them.
                        if (
                            $pattern instanceof TripleInterface &&
                            $pattern->getPredicate()->serialize() === 'rdfs:subClassOf*' &&
                            $pattern->getObject()->serialize() === 'schema:Role'
                        ) {
                            return true;
                        }
                    }
                    // If the mutation changes something other than a WebPage or Role, deny it.
                    return false;

                // Handle access for queries.
                case Constant::SECURITY_ATTRIBUTE_QUERY_MULTIPLE:
                case Constant::SECURITY_ATTRIBUTE_QUERY_SEARCH:
                case Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR:
                    /** @var ConditionalStatementInterface $statement */
                    $statement = $subject;
                    /** @var PatternInterface $pattern */
                    foreach ($statement->getConditions() as $pattern) {
                        // If the query asks for a WebPage, allow it.
                        if (
                            $pattern instanceof TripleInterface &&
                            $pattern->getPredicate()->serialize() === 'rdf:type' &&
                            $pattern->getObject()->serialize() === 'schema:WebPage'
                        ) {
                            return true;
                        }
                    }
                    // If the query asks for something other than a WebPage, deny it.
                    return false;

                case Constant::SECURITY_ATTRIBUTE_SUBQUERY:
                    /** @var ConditionalStatementInterface $statement */
                    $statement = $subject;
                    $access = false;
                    $identifier = null;
                    /** @var PatternInterface $pattern */
                    foreach ($statement->getConditions() as $pattern) {
                        if ($pattern instanceof OptionallyInterface) {
                            foreach ($pattern->toArray() as $subPattern) {
                                // If the query asks for a WebPage, allow it.
                                if (
                                    $subPattern instanceof TripleInterface &&
                                    $subPattern->getPredicate()->serialize() === 'rdf:type' &&
                                    $subPattern->getObject()->serialize() === 'schema:WebPage'
                                ) {
                                    $access = true;
                                }
                                // If the query asks for roleName, allow it
                                if (
                                    $subPattern instanceof TripleInterface &&
                                    $subPattern->getPredicate()->serialize() === 'schema:roleName'
                                ) {
                                    $access = true;
                                }
                                // If the query asks for headline, allow it
                                if (
                                    $subPattern instanceof TripleInterface &&
                                    $subPattern->getPredicate()->serialize() === 'schema:headline'
                                ) {
                                    $access = true;
                                }
                                if (
                                    $subPattern instanceof TripleInterface &&
                                    $subPattern->getPredicate()->serialize() === 'schema:identifier'
                                ) {
                                    $identifier = $subPattern->getObject()->getRawValue();
                                }
                                // If the query asks for url and the identifier matches a specific value, deny it
                                if (
                                    $subPattern instanceof TripleInterface &&
                                    $subPattern->getPredicate()->serialize() === 'schema:url' &&
                                    $identifier === 'edcda6e4-f36e-11eb-b63c-337058499d6c'
                                ) {
                                    $access = false;
                                }
                            }
                        }
                    }
                    // If the query asks for something other than a WebPage, deny it.
                    return $access;
            }
        }
        // Handle authenticated access.
        // In this example voter, authenticated access is granted.
        else {
            return true;
        }
        throw new LogicException('This should be an unreachable statement');
    }
}
