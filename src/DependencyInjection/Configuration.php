<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApi\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('schema_api');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('namespaces')
                    ->useAttributeAsKey('name')
                    ->scalarPrototype()->end()
                    ->defaultValue([])
                ->end() // namespaces
                ->booleanNode('use_bds_service')
                    ->defaultFalse()
                ->end() // use_bds_service
            ->end()
        ;
        return $treeBuilder;
    }
}
