<?php declare(strict_types = 1);

namespace EffectiveActivism\SchemaApi\DependencyInjection;

use EffectiveActivism\SchemaApi\Schema\Registry;
use EffectiveActivism\SchemaApi\Validation\ValidationInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SchemaApiExtension extends Extension
{
    /**
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $definition = $container->getDefinition(Registry::class);
        $definition->setArgument('$namespaces', $config['namespaces']);
        $definition->setArgument('$useBdsService', $config['use_bds_service']);
        $container->registerForAutoconfiguration(ValidationInterface::class)->addTag('schema_api.validation');
    }

    public function getAlias(): string
    {
        return 'schema_api';
    }
}
