<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema;

use DateTime;
use EffectiveActivism\SchemaApi\Constant;
use EffectiveActivism\SchemaApi\Schema\DataType\BooleanBox;
use EffectiveActivism\SchemaApi\Schema\DataType\DateTimeBox;
use EffectiveActivism\SchemaApi\Schema\DataType\DateBox;
use EffectiveActivism\SchemaApi\Schema\DataType\FloatBox;
use EffectiveActivism\SchemaApi\Schema\DataType\IntegerBox;
use EffectiveActivism\SchemaApi\Schema\DataType\NumberBox;
use EffectiveActivism\SchemaApi\Schema\DataType\TextBox;
use EffectiveActivism\SchemaApi\Schema\DataType\TimeBox;
use EffectiveActivism\SchemaApi\Schema\DataType\UrlBox;
use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\Helper\IriTrait;
use EffectiveActivism\SchemaApi\Validation\ValidationInterface;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use EffectiveActivism\SparQlClient\Exception\SparQlException;
use EffectiveActivism\SparQlClient\Syntax\Order\Asc;
use EffectiveActivism\SparQlClient\Syntax\Order\Desc;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Assignment\Values;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Filter;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\FilterNotExists;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\Equal;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\GreaterThan;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\GreaterThanOrEqual;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\LessThan;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\LessThanOrEqual;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Binary\NotEqual;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Unary\IsIri;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Constraint\Operator\Unary\IsLiteral;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Optionally\Optionally;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Service\Service;
use EffectiveActivism\SparQlClient\Syntax\Pattern\Triple\Triple;
use EffectiveActivism\SparQlClient\Syntax\Statement\SelectStatementInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\BlankNode\BlankNode;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\AbstractIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\AbstractLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\PlainLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Literal\TypedLiteral;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\InversePath;
use EffectiveActivism\SparQlClient\Syntax\Term\Path\ZeroOrMorePath;
use EffectiveActivism\SparQlClient\Syntax\Term\TermInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Variable\Variable;
use GraphQL\Error\InvariantViolation;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\String\Inflector\EnglishInflector;

class Registry
{
    use IriTrait;

    protected SparQlClientInterface $sparQlClient;

    protected array $descriptions = [];

    protected array $iris = [];

    protected array $namespaces = [];

    protected AuthorizationCheckerInterface $authorizationChecker;

    protected array $types = [];

    protected array $inputTypes = [];

    protected array $unions = [];

    protected bool $useBdsService = false;

    protected ValidationInterface $validator;

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    public function __construct(array $namespaces, bool $useBdsService, AuthorizationCheckerInterface $authorizationChecker, SparQlClientInterface $sparQlClient, $validatorObject)
    {
        $this->namespaces = $namespaces;
        $this->useBdsService = $useBdsService;
        $this->sparQlClient = $sparQlClient;
        $this->authorizationChecker = $authorizationChecker;
        /** @var RewindableGenerator validator */
        if (get_class($validatorObject) === RewindableGenerator::class) {
            $this->validator = $validatorObject->getIterator()->current();
        }
        // If no validator service is defined, use the NoValidation class.
        else {
            $this->validator = $validatorObject;
        }
        $classVariable = new Variable('class');
        $classesVariable = new Variable('classes');
        $commentVariable = new Variable('comment');
        $statement = $this->sparQlClient
            ->select([$classVariable, $commentVariable])
            ->where([
                new Triple($classVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('rdfs', 'Class')),
                // Do not include data types.
                new FilterNotExists([
                    new Triple($classVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('schema', 'DataType')),
                ]),
                // Do not include derived data types.
                new FilterNotExists([
                    new Triple($classVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $classesVariable),
                    new Triple($classesVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('schema', 'DataType')),
                ]),
                new Optionally([
                    new Triple($classVariable, new PrefixedIri('rdfs', 'comment'), $commentVariable),
                ]),
            ]);
        $sets = $this->sparQlClient->execute($statement);
        if ($sets !== false) {
            foreach ($sets as $set) {
                /** @var Iri $term */
                $term = $set[$classVariable->getVariableName()];
                $localPart = $this->getLocalPart($term->getRawValue());
                $this->types[$localPart] = null;
                $this->iris[$localPart] = $term->getRawValue();
                $this->descriptions[$localPart] = isset($set[$commentVariable->getVariableName()]) ? $set[$commentVariable->getVariableName()]->getRawValue() : null;
            }
        }
        // Set default input types.
        $this->inputTypes['Data'] = new InputObjectType([
            'name' => 'Data',
            'fields' => [
                'type' => Type::string(),
                'value' => Type::nonNull(Type::string()),
            ],
        ]);
        $this->inputTypes['Filter'] = new InputObjectType([
            'name' => 'Filter',
            'fields' => array_merge([
                Constant::FILTER_EQUAL_TO => Type::string(),
                Constant::FILTER_GREATER_THAN => Type::string(),
                Constant::FILTER_GREATER_THAN_OR_EQUAL_TO => Type::string(),
                Constant::FILTER_LESS_THAN => Type::string(),
                Constant::FILTER_LESS_THAN_OR_EQUAL_TO => Type::string(),
                Constant::FILTER_NOT_EQUAL_TO => Type::string(),
                Constant::FILTER_TYPE => Type::string(),
            ], $this->useBdsService ? [Constant::SEARCH_WILDCARD => Type::string()] : [])
        ]);
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    public function getType(string $iri): Type
    {
        $localPart = $this->getLocalPart($iri);
        // Append 'Box' to GraphQl reserved types to avoid collisions with GraphQl built-in types.
        // Also append 'Box' to Schema data types to normalize usage of 'Box' suffix.
        $localPart = in_array($localPart, array_merge(Constant::GRAPHQL_RESERVED_TYPES, Constant::SCHEMA_DATA_TYPES)) ? sprintf('%sBox', $localPart) : $localPart;
        if (!isset($this->types[$localPart])) {
            $this->iris[$localPart] = $iri;
            switch ($localPart) {
                case 'BooleanBox':
                    $this->types[$localPart] = new BooleanBox();
                    break;

                case 'DateBox':
                    $this->types[$localPart] = new DateBox();
                    break;

                case 'DateTimeBox':
                    $this->types[$localPart] = new DateTimeBox();
                    break;

                case 'FloatBox':
                    $this->types[$localPart] = new FloatBox();
                    break;

                case 'IntegerBox':
                    $this->types[$localPart] = new IntegerBox();
                    break;

                case 'NumberBox':
                    $this->types[$localPart] = new NumberBox();
                    break;

                case 'TextBox':
                    $this->types[$localPart] = new TextBox();
                    break;

                case 'TimeBox':
                    $this->types[$localPart] = new TimeBox();
                    break;

                case 'URLBox':
                    $this->types[$localPart] = new UrlBox();
                    break;

                default:
                    // Lower-case first letter is a schema.org property.
                    if (preg_match("/^[a-z]/", $localPart)) {
                        // All properties have an associated role.
                        $roleLocalPart = sprintf('%sRole', ucfirst($localPart));
                        $roleIri = sprintf('%s%s', Constant::BASE_ADDRESS, $roleLocalPart);
                        $this->types[$roleLocalPart] = $this->buildRoleClass($iri);
                        $this->iris[$roleLocalPart] = $roleIri;
                        $listOf = Type::listOf($this->buildProperty($iri));
                        $listOf->name = $this->getLocalPart($iri);
                        $this->types[$localPart] = $listOf;
                    }
                    // Numeric or Upper-case first letter is a schema.org class.
                    elseif (preg_match("/^[0-9A-Z]/", $localPart)) {
                        $this->types[$localPart] = $this->buildClass($iri);
                    }
                    else {
                        throw new SchemaApiException(sprintf('"%s" is an unknown class or property', $localPart));
                    }
                    break;
            }
        }
        return $this->types[$localPart];
    }

    public function getMutation(): ObjectType
    {
        return new ObjectType([
            'name' => 'Mutation',
            'fields' => function () {
                $fields = [];
                foreach ($this->types as $localPart => $data) {
                    // Only allow mutations for classes, not properties.
                    // And only allow mutations for classes that aren't data types.
                    if (
                        preg_match("/^[A-Z]/", $localPart) &&
                        !preg_match("/Box$/", $localPart)
                    ) {
                        $iri = $this->getIri($localPart);
                        // 'Insert' field.
                        $fields[sprintf('insert%s', $localPart)] = [
                            'description' => isset($this->descriptions[$localPart]) ? $this->descriptions[$localPart] : '',
                            'type' => $this->getType($iri),
                            'args' => [
                                'values' => [
                                    'type' => Type::nonNull($this->buildArguments($iri)),
                                    'description' => 'The values to insert',
                                ],
                            ],
                            'resolve' => function($value, array $arguments, $context, ResolveInfo $info) use ($iri) {
                                $triples = [];
                                $conditions = [];
                                // Identifiers will be overwritten upon creation to avoid collisions with existing
                                // data and guarantee uniform identifier format.
                                $identifierString = Uuid::uuid4()->toString();
                                $arguments['values']['identifier'] = [
                                    'value' => $identifierString,
                                ];
                                $subject = new Iri(sprintf('urn:uuid:%s', $identifierString));
                                // Create type manually.
                                $triples[] = new Triple($subject, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($iri));
                                // Create all other types.
                                $subjectVariable = new Variable('subject');
                                foreach ($arguments['values'] as $property => $data) {
                                    $newValueVariable = new Variable(sprintf('newValueFor%s', ucfirst($property)));
                                    $argumentIri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($argumentIri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listOfType */
                                    $listOfType = $this->getType($argumentIri);
                                    /** @var Type $type */
                                    foreach ($listOfType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $objects = $this->resolveArgumentValues($types, $data);
                                    $values = [];
                                    foreach ($objects as $object) {
                                        $values[] = [$object];
                                    }
                                    $conditions[] = new Values([$newValueVariable], $values);
                                    $triples[] = new Triple($subject, $predicate, $newValueVariable);
                                }
                                $statement = $this->sparQlClient->insert($triples)->where($conditions);
                                $filters = [
                                    new Triple($subjectVariable, new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifierString)),
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($iri)),
                                ];
                                $validationResult = $this->validator->validate($statement);
                                if (!$validationResult->getStatus()) {
                                    throw new SchemaApiException('Validation failed. ' . implode(' ', $validationResult->getMessages()));
                                }
                                if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_MUTATION_INSERT, $statement)) {
                                    throw new SchemaApiException('Operation not allowed');
                                }
                                $this->sparQlClient->execute($statement);
                                $resolveContainer = new ResolveContainer();
                                return $resolveContainer
                                    ->setFilters($filters);
                            },
                        ];
                        // 'Update' field.
                        $fields[sprintf('update%s', $localPart)] = [
                            'description' => isset($this->descriptions[$localPart]) ? $this->descriptions[$localPart] : '',
                            'type' => $this->getType($iri),
                            'args' => [
                                'filters' => [
                                    'type' => Type::nonNull($this->buildFilters($iri)),
                                    'description' => 'The values to filter by',
                                ],
                                'values' => [
                                    'type' => Type::nonNull($this->buildArguments($iri)),
                                    'description' => 'The values to update',
                                ],
                                'overwrite' => [
                                    'type' => Type::boolean(),
                                    'description' => 'If set to true, overwrites existing values with new values. Defaults to \'false\'',
                                    'defaultValue' => false,
                                ],
                            ],
                            'resolve' => function($value, array $arguments, $context, ResolveInfo $info) use ($iri) {
                                $originalTriples = [];
                                $replacementTriples = [];
                                $conditions = [];
                                $bnodes = [];
                                $subjectVariable = new Variable('subject');
                                $objectVariable = new Variable('object');
                                $filters = [
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($iri)),
                                ];
                                // First, create the filters.
                                foreach ($arguments['filters'] as $property => $data) {
                                    $argumentIri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($argumentIri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listOfType */
                                    $listOfType = $this->getType($argumentIri);
                                    /** @var Type $type */
                                    foreach ($listOfType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $filters = array_merge($filters, $this->resolveFilterValues($subjectVariable, $predicate, $types, $data));
                                }
                                // Then, create the replace statements.
                                // Do not allow changes to identifier.
                                unset($arguments['values']['identifier']);
                                foreach ($arguments['values'] as $property => $data) {
                                    $newValueVariable = new Variable(sprintf('newValueFor%s', ucfirst($property)));
                                    $argumentIri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($argumentIri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listOfType */
                                    $listOfType = $this->getType($argumentIri);
                                    /** @var Type $type */
                                    foreach ($listOfType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $objects = $this->resolveArgumentValues($types, $data);
                                    $values = [];
                                    foreach ($objects as $object) {
                                        $values[] = [$object];
                                    }
                                    $conditions[] = new Values([$newValueVariable], $values);
                                    if ($arguments['overwrite']) {
                                        $conditions[] = new Triple($subjectVariable, $predicate, $objectVariable);
                                        $originalTriples[] = new Triple($subjectVariable, $predicate, $objectVariable);
                                    }
                                    $replacementTriples[] = new Triple($subjectVariable, $predicate, $newValueVariable);
                                    $bnodes[] = new Triple($subjectVariable, $predicate, new BlankNode('placeholder'));
                                }
                                if ($arguments['overwrite']) {
                                    // This statement will replace existing values, but will not insert new ones.
                                    // Because the bnode insert statement below ensures a placeholder bnode, this
                                    // statement will always replace existing data.
                                    $statement = $this->sparQlClient
                                        ->replace($originalTriples)
                                        ->with($replacementTriples)
                                        ->where(array_merge($filters, $conditions));
                                }
                                else {
                                    $statement = $this->sparQlClient
                                        ->insert($replacementTriples)
                                        ->where(array_merge($filters, $conditions));
                                }
                                $validationResult = $this->validator->validate($statement);
                                if (!$validationResult->getStatus()) {
                                    throw new SchemaApiException('Validation failed. ' . implode(' ', $validationResult->getMessages()));
                                }
                                if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_MUTATION_UPDATE, $statement)) {
                                    throw new SchemaApiException('Operation not allowed');
                                }
                                if ($arguments['overwrite']) {
                                    // Insert bnodes to ensure that there is a value to replace.
                                    $this->sparQlClient->execute($this->sparQlClient->insert($bnodes)->where($filters));
                                }
                                // At this point, if the request is set to overwrite, bnodes have already been created
                                // to ensure that there are values to replace.
                                $this->sparQlClient->execute($statement);
                                $resolveContainer = new ResolveContainer();
                                return $resolveContainer
                                    ->setFilters($filters);
                            },
                        ];
                        // 'Delete' field.
                        $deleteLocalPart = sprintf('delete%s', $localPart);
                        $fields[$deleteLocalPart] = [
                            'description' => isset($this->descriptions[$localPart]) ? $this->descriptions[$localPart] : '',
                            'type' => Type::boolean(),
                            'args' => [
                                'filters' => [
                                    'type' => Type::nonNull($this->buildFilters($iri)),
                                    'description' => 'The values to filter by',
                                ],
                                'onlyDelete' => [
                                    'type' => $this->buildArguments($iri),
                                    'description' => 'Optionally specify the only values that should be deleted',
                                ],
                            ],
                            'resolve' => function($value, array $arguments, $context, ResolveInfo $info) use ($iri) {
                                $triples = [];
                                $subjectVariable = new Variable('subject');
                                $predicateVariable = new Variable('predicate');
                                $objectVariable = new Variable('object');
                                // Always set type. This ensures that data belonging to other types cannot be
                                // retrieved by using their identifiers.
                                $filters = [
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($iri)),
                                ];
                                foreach ($arguments['filters'] as $property => $data) {
                                    $argumentIri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($argumentIri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listType */
                                    $listOfType = $this->getType($argumentIri);
                                    /** @var Type $type */
                                    foreach ($listOfType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $filters = array_merge($filters, $this->resolveFilterValues($subjectVariable, $predicate, $types, $data));
                                }
                                if (!empty($arguments['onlyDelete'])) {
                                    foreach ($arguments['onlyDelete'] as $property => $data) {
                                        $argumentIri = $this->getIri($property);
                                        $predicate = $this->attemptCreatePrefixedIri($argumentIri);
                                        // Resolve argument type.
                                        $types = [];
                                        /** @var ListOfType $listType */
                                        $listOfType = $this->getType($argumentIri);
                                        /** @var Type $type */
                                        foreach ($listOfType->ofType->config['types'] as $type) {
                                            $types[] = $type->name;
                                        }
                                        $objects = $this->resolveArgumentValues($types, $data);
                                        foreach ($objects as $object) {
                                            $triples[] = new Triple($subjectVariable, $predicate, $object);
                                        }
                                    }
                                }
                                else {
                                    $triple = new Triple($subjectVariable, $predicateVariable, $objectVariable);
                                    $triples[] = $triple;
                                    $filters[] = $triple;
                                    // Also delete any references to this subject.
                                    $referencingSubjectVariable = new Variable('referencingSubject');
                                    $referencingPredicateVariable = new Variable('referencingPredicate');
                                    $referencingTriple = new Triple($referencingSubjectVariable, $referencingPredicateVariable, $subjectVariable);
                                    $triples[] = $referencingTriple;
                                    $filters[] = $referencingTriple;
                                }
                                $statement = $this->sparQlClient
                                    ->delete($triples)
                                    ->where($filters);
                                $validationResult = $this->validator->validate($statement);
                                if (!$validationResult->getStatus()) {
                                    throw new SchemaApiException('Validation failed. ' . implode(' ', $validationResult->getMessages()));
                                }
                                if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_MUTATION_DELETE, $statement)) {
                                    throw new SchemaApiException('Operation not allowed');
                                }
                                $this->sparQlClient->execute($statement);
                                return true;
                            },
                        ];
                    }
                }
                return $fields;
            },
        ]);
    }

    public function getQuery(): ObjectType
    {
        return new ObjectType([
            'name' => 'Query',
            'fields' => function () {
                $fields = [];
                foreach ($this->types as $localPart => $data) {
                    // 'Get' field singular.
                    $fields[sprintf('get%s', $localPart)] = [
                        'description' => isset($this->descriptions[$localPart]) ? $this->descriptions[$localPart] : '',
                        'type' => $this->getType($this->getIri($localPart)),
                        'args' => [
                            'filters' => [
                                'type' => $this->buildFilters($this->getIri($localPart)),
                                'description' => 'The values to filter by.',
                            ],
                        ],
                        'resolve' => function($value, array $arguments, $context, ResolveInfo $info) use ($localPart) {
                            $subjectVariable = new Variable('subject');
                            // Always set type. This ensures that data belonging to other types cannot be
                            // retrieved by using their identifiers.
                            $filters = [
                                new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($this->getIri($localPart))),
                            ];
                            if (isset($arguments['filters'])) {
                                foreach ($arguments['filters'] as $property => $data) {
                                    $iri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($iri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listType */
                                    $listType = $this->getType($iri);
                                    /** @var Type $type */
                                    foreach ($listType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $filters = array_merge($filters, $this->resolveFilterValues($subjectVariable, $predicate, $types, $data));
                                }
                            }
                            // Look up the first match.
                            $statement = $this->sparQlClient
                                ->select([$subjectVariable])
                                ->limit(1)
                                ->where($filters);
                            if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_QUERY_SINGULAR, $statement)) {
                                throw new SchemaApiException('Operation not allowed');
                            }
                            $sets = $this->sparQlClient->execute($statement);
                            // If matched, continue unfolding the request using the first match.
                            if (isset($sets[0])) {
                                /** @var TermInterface $term */
                                $term = $sets[0][$subjectVariable->getVariableName()];
                                $identifier = str_replace('urn:uuid:', '', $term->getRawValue());
                                $resolveContainer = new ResolveContainer();
                                $resolveContainer->setFilters([
                                    new Triple(new Variable('subject'), new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifier)),
                                    // Always set type. This ensures that data belonging to other types cannot be
                                    // retrieved by using their identifiers.
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($this->getIri($localPart))),
                                ]);
                                return $resolveContainer;
                            }
                            // If not matched, stop propagation.
                            return null;
                        },
                    ];
                    // 'Get' field plural.
                    $inflector = new EnglishInflector();
                    $pluralLocalParts = $inflector->pluralize($localPart);
                    list($pluralLocalPart,) = $pluralLocalParts;
                    $fields[sprintf('get%s', $pluralLocalPart)] = [
                        'description' => isset($this->descriptions[$localPart]) ? $this->descriptions[$localPart] : '',
                        'type' => Type::listOf($this->getType($this->getIri($localPart))),
                        'args' => [
                            'filters' => [
                                'type' => $this->buildFilters($this->getIri($localPart)),
                                'description' => 'The values to filter by.',
                            ],
                            'limit' => [
                                'type' => Type::int(),
                                'description' => 'How many results to return',
                                'defaultValue' => 0,
                            ],
                            'offset' => [
                                'type' => Type::int(),
                                'description' => 'The offset to return results from',
                                'defaultValue' => 0,
                            ],
                            'orderBy' => [
                                'type' => Type::string(),
                                'description' => 'What field to order results by',
                            ],
                            'orderAscending' => [
                                'type' => Type::boolean(),
                                'description' => 'Order results ascending or descending',
                                'defaultValue' => true,
                            ],
                        ],
                        'resolve' => function($value, array $arguments, $context, ResolveInfo $info) use ($localPart) {
                            $subjectVariable = new Variable('subject');
                            $filters = [
                                // Always set type. This ensures that data belonging to other types cannot be
                                // retrieved by using their identifiers.
                                new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($this->getIri($localPart))),
                            ];
                            if (isset($arguments['filters'])) {
                                foreach ($arguments['filters'] as $property => $data) {
                                    $iri = $this->getIri($property);
                                    $predicate = $this->attemptCreatePrefixedIri($iri);
                                    // Resolve argument type.
                                    $types = [];
                                    /** @var ListOfType $listOfType */
                                    $listOfType = $this->getType($iri);
                                    /** @var Type $type */
                                    foreach ($listOfType->ofType->config['types'] as $type) {
                                        $types[] = $type->name;
                                    }
                                    $filters = array_merge($filters, $this->resolveFilterValues($subjectVariable, $predicate, $types, $data));
                                }
                            }
                            $statement = $this->sparQlClient
                                ->select([$subjectVariable])
                                ->limit($arguments['limit'])
                                ->offset($arguments['offset'])
                                ->where($filters);
                            if (isset($arguments['orderBy'])) {
                                // Check if type has field matching orderBy value.
                                try {
                                    $field = $info->returnType->getOfType()->getField($arguments['orderBy']);
                                    // If so (no exception was thrown), add triple to conditions to link orderBy with statement.
                                    $sortByProperty = $this->attemptCreatePrefixedIri($this->getIri($field->getName()));
                                    $conditions = $statement->getConditions();
                                    $orderByVariable = new Variable('orderBy');
                                    $conditions[] = new Triple($subjectVariable, $sortByProperty, $orderByVariable);
                                    $direction = $arguments['orderAscending'] ? new Asc($orderByVariable) : new Desc($orderByVariable);
                                    $statement
                                        ->orderBy([$direction])
                                        ->where($conditions);
                                } catch (InvariantViolation) {
                                    throw new SchemaApiException(sprintf('%s does not have field %s to order by', $info->returnType->getOfType()->name, $arguments['orderBy']));
                                }
                            }
                            if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_QUERY_MULTIPLE, $statement)) {
                                throw new SchemaApiException('Operation not allowed');
                            }
                            $sets = $this->sparQlClient->execute($statement);
                            $resolveContainers = [];
                            foreach ($sets as $set) {
                                /** @var TermInterface $term */
                                $term = $set[$subjectVariable->getVariableName()];
                                $identifier = str_replace('urn:uuid:', '', $term->getRawValue());
                                $resolveContainer = new ResolveContainer();
                                $resolveContainer->setFilters([
                                    new Triple(new Variable('subject'), new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifier)),
                                    // Always set type. This ensures that data belonging to other types cannot be
                                    // retrieved by using their identifiers.
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($this->getIri($localPart))),
                                ]);
                                $resolveContainers[] = $resolveContainer;
                            }
                            return array_unique($resolveContainers, SORT_REGULAR);
                        },
                    ];
                }
                return $fields;
            },
        ]);
    }

    public function getUnion(string $unionName)
    {
        if (!isset($this->unions[$unionName])) {
            throw new SchemaApiException(sprintf('"%s" is not found in union registry', $unionName));
        }
        return $this->unions[$unionName];
    }

    /**
     * @throws SchemaApiException
     */
    protected function buildArguments(string $iri): InputObjectType
    {
        $localPart = sprintf('ArgumentsFor%s', $this->getLocalPart($iri));
        if (!isset($this->inputTypes[$localPart])) {
            $this->inputTypes[$localPart] = new InputObjectType([
                'name' => $localPart,
                'description' => sprintf('Arguments for the %s type', $this->getLocalPart($iri)),
                'fields' => function () use ($iri) {
                    $fields = [];
                    $type = $this->getType($iri);
                    if (
                        isset($type->config['fields']) &&
                        is_callable($type->config['fields'])
                    ) {
                        $callable = $type->config['fields'];
                        $fields = $callable();
                        foreach ($fields as $localPart => $data) {
                            $fields[$localPart] = [
                                'type' => Type::listOf($this->inputTypes['Data']),
                                'description' => isset($data['description']) ? $data['description'] : '',
                            ];
                        }
                    }
                    return $fields;
                },
            ]);
        }
        return $this->inputTypes[$localPart];
    }

    /**
     * @throws SchemaApiException
     */
    protected function buildFilters(string $iri): InputObjectType
    {
        $localPart = sprintf('FiltersFor%s', $this->getLocalPart($iri));
        if (!isset($this->inputTypes[$localPart])) {
            $this->inputTypes[$localPart] = new InputObjectType([
                'name' => $localPart,
                'description' => sprintf('Filters for the %s type', $this->getLocalPart($iri)),
                'fields' => function () use ($iri) {
                    $fields = [];
                    $type = $this->getType($iri);
                    if (
                        isset($type->config['fields']) &&
                        is_callable($type->config['fields'])
                    ) {
                        $callable = $type->config['fields'];
                        $fields = $callable();
                        foreach ($fields as $localPart => $data) {
                            $fields[$localPart] = [
                                'type' => Type::listOf($this->inputTypes['Filter']),
                                'description' => isset($data['description']) ? $data['description'] : '',
                            ];
                        }
                    }
                    return $fields;
                },
            ]);
        }
        return $this->inputTypes[$localPart];
    }

    /**
     * @throws SchemaApiException
     */
    protected function buildClass(string $iri): ObjectType
    {
        $localPart = $this->getLocalPart($iri);
        return new ObjectType([
            'name' => $localPart,
            'description' => $this->descriptions[$localPart],
            'fields' => function () use ($iri) {
                $fields = [];
                $propertyVariable = new Variable('property');
                $classesVariable = new Variable('classes');
                $commentVariable = new Variable('comment');
                $classTerm = $this->attemptCreatePrefixedIri($iri);
                $statement = $this->sparQlClient
                    ->select([$propertyVariable, $commentVariable])
                    ->where([
                        new Triple($classTerm, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $classesVariable),
                        new Triple($classesVariable, new InversePath(new PrefixedIri('schema', 'domainIncludes')), $propertyVariable),
                        new Optionally([new Triple($propertyVariable, new PrefixedIri('rdfs', 'comment'), $commentVariable)]),
                        // Do not include pending properties.
                        new FilterNotExists([
                            new Triple($propertyVariable, new PrefixedIri('schema', 'isPartOf'), new Iri(Constant::PENDING_ADDRESS)),
                        ]),
                    ]);
                $sets = $this->sparQlClient->execute($statement);
                foreach ($sets as $set) {
                    /** @var Iri $propertyAsIri */
                    $propertyAsIri = $set[$propertyVariable->getVariableName()];
                    /** @var AbstractIri $property */
                    $property = $this->attemptConvertToPrefixedIri($propertyAsIri);
                    /** @var TermInterface $description */
                    $description = $set[$commentVariable->getVariableName()];
                    $localPart = $this->getLocalPart($propertyAsIri->getRawValue());
                    $this->descriptions[$localPart] = $description->getRawValue();
                    $fields[$localPart] = [
                        'type' => $this->getType($propertyAsIri->getRawValue()),
                        'description' => $description->getRawValue(),
                        'resolve' => function(ResolveContainer $resolveContainer, array $arguments, $context, ResolveInfo $info) use ($property) {
                            $subjectVariable = new Variable('subject');
                            $isIriVariable = new Variable('isIri');
                            $isLiteralVariable = new Variable('isLiteral');
                            $isClassVariable = new Variable('isClass');
                            $potentialClassVariable = new Variable('potentialClass');
                            $isRoleVariable = new Variable('isRole');
                            $potentialRoleClassVariable = new Variable('potentialRoleClass');
                            /** @var SelectStatementInterface $statement */
                            $statement = $this->sparQlClient
                                // The order of variables in this select statement is significant, as Iris are nearly
                                // always a match.
                                ->select([$isRoleVariable, $isClassVariable, $isLiteralVariable, $isIriVariable])
                                ->where([
                                    // Look for Role.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isRoleVariable),
                                        new Triple($isRoleVariable, new PrefixedIri('rdf', 'type'), $potentialRoleClassVariable),
                                        new Triple($potentialRoleClassVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), new PrefixedIri('schema', 'Role')),
                                    ])),
                                    // Look for classes.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isClassVariable),
                                        new Triple($isClassVariable, new PrefixedIri('rdf', 'type'), $potentialClassVariable),
                                        new Triple($potentialClassVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('rdfs', 'Class')),
                                    ])),
                                    // Look for literals.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isLiteralVariable),
                                        new Filter(new IsLiteral($isLiteralVariable)),
                                    ])),
                                    // Look for iris.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isIriVariable),
                                        new Filter(new IsIri($isIriVariable)),
                                    ])),
                                ]);
                            if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_SUBQUERY, $statement)) {
                                throw new SchemaApiException('Operation not allowed');
                            }
                            $sets = $this->sparQlClient->execute($statement);
                            $resolveContainers = [];
                            foreach ($sets as $set) {
                                $setResolveContainer = clone $resolveContainer;
                                foreach ($set as $variableName => $term) {
                                    // Role matches are also matched by Iri filter. However, since the 'isRole' variable
                                    // is first in the above query, it will always be prioritized ahead of 'isIri'.
                                    if ($term !== null) {
                                        $setResolveContainer->setValue($term);
                                        $setResolveContainer->setProperty($property);
                                        break;
                                    }
                                }
                                $resolveContainers[] = $setResolveContainer;
                            }
                            return array_unique($resolveContainers, SORT_REGULAR);
                        },
                    ];
                }
                return $fields;
            },
        ]);
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    protected function buildProperty(string $iri): UnionType
    {
        $localPart = $this->getLocalPart($iri);
        $typeVariable = new Variable('type');
        $statement = $this->sparQlClient
            ->select([$typeVariable])
            ->where([
                new Triple($this->attemptCreatePrefixedIri($iri), new PrefixedIri('schema', 'rangeIncludes'), $typeVariable),
                // Do not include pending types.
                new FilterNotExists([
                    new Triple($typeVariable, new PrefixedIri('schema', 'isPartOf'), new Iri(Constant::PENDING_ADDRESS)),
                ]),
            ]);
        $sets = $this->sparQlClient->execute($statement);
        // Create types for property.
        $types = [];
        if ($sets !== false) {
            foreach ($sets as $set) {
                /** @var Iri $term */
                $term = $set[$typeVariable->getVariableName()];
                $types[] = $this->getType($term->getRawValue());
            }
        }
        // Always include the 'Role' property. For more information, see
        // http://blog.schema.org/2014/06/introducing-role.html and the
        // README of this bundle.
        $types[] = $this->getType(sprintf('%s%sRole', Constant::BASE_ADDRESS, ucfirst($localPart)));
        $uniqueTypes = array_unique($types, SORT_REGULAR);
        sort($uniqueTypes);
        $unionType = new UnionType([
            'name' => sprintf('UnionOf%s', ucfirst($localPart)),
            'description' => sprintf('Union of the %s property', $localPart),
            'types' => $uniqueTypes,
            'resolveType' => function (ResolveContainer &$resolveContainer) use ($iri, $localPart, $uniqueTypes) {
                $subjectVariable = new Variable('subject');
                $isClassVariable = new Variable('isClass');
                $isIriVariable = new Variable('isIri');
                $isLiteralVariable = new Variable('isLiteral');
                $isRoleVariable = new Variable('isRole');
                $roleClassVariable = new Variable('roleClass');
                $type = null;
                $term = $resolveContainer->getValue();
                switch ($term->getVariableName()) {
                    case $isIriVariable->getVariableName():
                        $type = $this->getType(sprintf('%sURL', Constant::BASE_ADDRESS));
                        break;

                    case $isLiteralVariable->getVariableName():
                        /** @var AbstractLiteral $term */
                        $type = $this->determineLiteralType($term);
                        break;

                    case $isRoleVariable->getVariableName():
                        $identifier = str_replace('urn:uuid:', '', $term->getRawValue());
                        $resolveContainer
                            ->setFilters([
                                new Triple($subjectVariable, new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifier)),
                                // Always set type. This ensures that data belonging to other types cannot be
                                // retrieved by using their identifiers.
                                new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $roleClassVariable),
                                new Triple($roleClassVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), new PrefixedIri('schema', 'Role')),
                            ])
                            ->setIncomingProperty($this->attemptCreatePrefixedIri($iri));
                        $typeIri = sprintf('%s%sRole', Constant::BASE_ADDRESS, ucfirst($localPart));
                        $type = $this->getType($typeIri);
                        break;

                    case $isClassVariable->getVariableName():
                        $identifier = str_replace('urn:uuid:', '', $term->getRawValue());
                        // At this point, type is unknown, so it must be retrieved.
                        $classTypeVariable = new Variable('classType');
                        // Also retrieve parent classes, to check if any allowed types match with those.
                        $parentClassTypeVariable = new Variable('parentClassType');
                        $statement = $this->sparQlClient
                            ->select([$classTypeVariable, $parentClassTypeVariable])
                            ->where([
                                new Triple($subjectVariable, new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifier)),
                                new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $classTypeVariable),
                                new Optionally([
                                    new Triple($classTypeVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $parentClassTypeVariable),
                                ]),
                            ]);
                        $sets = $this->sparQlClient->execute($statement);
                        if (!empty($sets)) {
                            /** @var Iri $classTypeTermAsIri */
                            $classTypeTermAsIri = $sets[0][$classTypeVariable->getVariableName()];
                            /** @var AbstractIri $classTypeTerm */
                            $classTypeTerm = $this->attemptConvertToPrefixedIri($classTypeTermAsIri);
                            $resolveContainer
                                ->setFilters([
                                    new Triple($subjectVariable, new PrefixedIri('schema', 'identifier'), new PlainLiteral($identifier)),
                                    // Always set type. This ensures that data belonging to other types cannot be
                                    // retrieved by using their identifiers.
                                    new Triple($subjectVariable, new PrefixedIri('rdf', 'type'), $classTypeTerm),
                                ]);
                            $typeIri = $classTypeTermAsIri->getRawValue();
                            $type = $this->getType($typeIri);
                            // Check if type is present in UnionOf... allowed types. If not, try any parent class types.
                            if (!in_array($type->name, array_map(function (Type $uniqueType) {
                                return $uniqueType->name;
                            }, $uniqueTypes))) {
                                // Iterate parentClassType, if any.
                                foreach ($sets as $set) {
                                    if (isset($set[$parentClassTypeVariable->getVariableName()])) {
                                        /** @var AbstractIri $parentClassTypeTerm */
                                        $parentClassTypeTerm = $this->attemptConvertToPrefixedIri($set[$parentClassTypeVariable->getVariableName()]);
                                        // If UnionOf... has parentClassType as type, use that as type instead.
                                        /** @var Type $uniqueType */
                                        foreach ($uniqueTypes as $uniqueType) {
                                            if (
                                                ($parentClassTypeTerm instanceof Iri && $uniqueType->name === $this->getLocalPart($parentClassTypeTerm->getRawValue())) ||
                                                ($parentClassTypeTerm instanceof PrefixedIri && $uniqueType->name === $parentClassTypeTerm->getLocalPart())
                                            ) {
                                                $type = $uniqueType;
                                                break 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            throw new SchemaApiException(sprintf('No content found for iri "%s"', $identifier));
                        }
                        break;
                }
                if ($type === null) {
                    throw new SchemaApiException(sprintf('Unresolved type "%s"', $localPart));
                }
                return $type;
            },
        ]);
        $this->unions[$unionType->name] = $unionType;
        return $unionType;
    }

    /**
     * @throws SchemaApiException
     */
    protected function buildRoleClass(string $incomingIri): ObjectType
    {
        $incomingLocalPart = $this->getLocalPart($incomingIri);
        return new ObjectType([
            'name' => sprintf('%sRole', ucfirst($incomingLocalPart)),
            'description' => sprintf('Role for the %s property', $incomingLocalPart),
            'fields' => function () use ($incomingIri, $incomingLocalPart) {
                $fields = [];
                $propertyVariable = new Variable('property');
                $classesVariable = new Variable('classes');
                $commentVariable = new Variable('comment');
                // TODO: If the role already exists, fx. 'OrganizationRole', use that instead.
                $classTerm = new PrefixedIri(Constant::BASE_PREFIX, 'Role');
                $statement = $this->sparQlClient
                    ->select([$propertyVariable, $commentVariable])
                    ->where([
                        new Triple($classTerm, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), $classesVariable),
                        new Triple($classesVariable, new InversePath(new PrefixedIri('schema', 'domainIncludes')), $propertyVariable),
                        new Optionally([
                            new Triple($propertyVariable, new PrefixedIri('rdfs', 'comment'), $commentVariable),
                        ]),
                        // Do not include pending properties.
                        new FilterNotExists([
                            new Triple($propertyVariable, new PrefixedIri('schema', 'isPartOf'), new Iri(Constant::PENDING_ADDRESS)),
                        ]),
                    ]);
                $sets = $this->sparQlClient->execute($statement);
                $roleFields = [];
                if ($sets !== false) {
                    foreach ($sets as $set) {
                        /** @var AbstractIri $property */
                        $property = $this->attemptConvertToPrefixedIri($set[$propertyVariable->getVariableName()]);
                        $roleFieldLocalPart = $property instanceof PrefixedIri ? $property->getLocalPart() : $this->getLocalPart($property->getRawValue());
                        /** @var TermInterface $description */
                        $description = $set[$commentVariable->getVariableName()];
                        $roleFields[] = [$property, $description, $roleFieldLocalPart];
                    }
                }
                // Avoid overwriting fields that exist in Roles, such as endDate and roleName.
                $sanitizedIncomingLocalPart = $incomingLocalPart;
                foreach ($roleFields as $roleField) {
                    list(,,$roleFieldLocalPart) = $roleField;
                    if ($roleFieldLocalPart === $incomingLocalPart) {
                        $sanitizedIncomingLocalPart = sprintf('_%s', $incomingLocalPart);
                        break;
                    }
                }
                // Add the incoming property.
                $roleFields[] = [
                    $this->attemptCreatePrefixedIri($incomingIri),
                    new PlainLiteral(sprintf('The incoming property "%s"', $incomingLocalPart)),
                    $sanitizedIncomingLocalPart,
                ];
                foreach ($roleFields as $roleField) {
                    list($property, $description, $roleFieldLocalPart) = $roleField;
                    $this->descriptions[$roleFieldLocalPart] = $description->getRawValue();
                    $fields[$roleFieldLocalPart] = [
                        'type' => $this->getType($property instanceof PrefixedIri ? $this->createIriFromPrefixedIri($property)->getRawValue() : $property->getRawValue()),
                        'description' => $description->getRawValue(),
                        'resolve' => function(ResolveContainer $resolveContainer, array $arguments, $context, ResolveInfo $info) use ($property) {
                            $subjectVariable = new Variable('subject');
                            $isIriVariable = new Variable('isIri');
                            $isLiteralVariable = new Variable('isLiteral');
                            $isClassVariable = new Variable('isClass');
                            $potentialClassVariable = new Variable('potentialClass');
                            $isRoleVariable = new Variable('isRole');
                            $potentialRoleClassVariable = new Variable('potentialRoleClass');
                            $statement = $this->sparQlClient
                                // The order of variables in this select statement is significant, as Iris are nearly
                                // always a match.
                                ->select([$isRoleVariable, $isClassVariable, $isLiteralVariable, $isIriVariable])
                                ->where([
                                    // Look for Role.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isRoleVariable),
                                        new Triple($isRoleVariable, new PrefixedIri('rdf', 'type'), $potentialRoleClassVariable),
                                        new Triple($potentialRoleClassVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), new PrefixedIri('schema', 'Role')),
                                    ])),
                                    // Look for classes.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isClassVariable),
                                        new Triple($isClassVariable, new PrefixedIri('rdf', 'type'), $potentialClassVariable),
                                        new Triple($potentialClassVariable, new PrefixedIri('rdf', 'type'), new PrefixedIri('rdfs', 'Class')),
                                    ])),
                                    // Look for literals.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isLiteralVariable),
                                        new Filter(new IsLiteral($isLiteralVariable)),
                                    ])),
                                    // Look for iris.
                                    new Optionally(array_merge($resolveContainer->getFilters(), [
                                        new Triple($subjectVariable, $property, $isIriVariable),
                                        new Filter(new IsIri($isIriVariable)),
                                    ])),
                                ]);
                            if (!$this->authorizationChecker->isGranted(Constant::SECURITY_ATTRIBUTE_SUBQUERY, $statement)) {
                                throw new SchemaApiException('Operation not allowed');
                            }
                            $sets = $this->sparQlClient->execute($statement);
                            $resolveContainers = [];
                            foreach ($sets as $set) {
                                $setResolveContainer = clone $resolveContainer;
                                foreach ($set as $variableName => $term) {
                                    // Role matches are also matched by Iri filter. However, since the 'isRole' variable
                                    // is first in the above query, it will always be prioritized ahead of 'isIri'.
                                    if ($term !== null) {
                                        $setResolveContainer->setValue($term);
                                        $setResolveContainer->setProperty($property);
                                        break;
                                    }
                                }
                                $resolveContainers[] = $setResolveContainer;
                            }
                            return array_unique($resolveContainers, SORT_REGULAR);
                        },
                    ];
                }
                return $fields;
            },
        ]);
    }

    /**
     * @throws SchemaApiException
     */
    protected function determineLiteralType(AbstractLiteral $value): ?Type
    {
        $type = null;
        try {
            // Attempt to serialize value. This may cause exceptions.
            $value->serialize();
            switch ($value->getType()) {
                case 'xsd:boolean':
                    $type = $this->getType(sprintf('%sBoolean', Constant::BASE_ADDRESS));
                    break;

                case 'xsd:date':
                    if (DateTime::createFromFormat(Constant::XSD_DATE_FORMATS['date'], $value->getRawValue()) !== false) {
                        $type = $this->getType(sprintf('%sDate', Constant::BASE_ADDRESS));
                    }
                    break;

                case 'xsd:dateTime':
                    if (DateTime::createFromFormat(Constant::XSD_DATE_FORMATS['dateTime'], $value->getRawValue()) !== false) {
                        $type = $this->getType(sprintf('%sDateTime', Constant::BASE_ADDRESS));
                    }
                    break;

                case 'xsd:decimal':
                    // Schema.org uses schema.org/Number in favor of schema.org/Float (which is defined, but unused).
                    $type = $this->getType(sprintf('%sNumber', Constant::BASE_ADDRESS));
                    break;

                case 'xsd:integer':
                    $type = $this->getType(sprintf('%sInteger', Constant::BASE_ADDRESS));
                    break;

                case 'xsd:string':
                    $type = $this->getType(sprintf('%sText', Constant::BASE_ADDRESS));
                    break;

                case 'xsd:time':
                    if (DateTime::createFromFormat(Constant::XSD_DATE_FORMATS['time'], $value->getRawValue()) !== false) {
                        $type = $this->getType(sprintf('%sTime', Constant::BASE_ADDRESS));
                    }
                    break;

            }
        } catch (SparQlException $exception) {
            throw new SchemaApiException($exception->getMessage());
        }
        return $type;
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    protected function resolveArgumentValues(array $types, array $data): array
    {
        $terms = [];
        $children = array_values($data);
        $child = array_pop($children);
        if (is_array($child)) {
            foreach ($data as $datum) {
                $valueType = isset($datum['type']) ? $datum['type'] : null;
                unset($datum['type']);
                $value = array_pop($datum);
                $terms[] = $this->resolveArgumentValue($types, $value, $valueType);
            }
        }
        else {
            $valueType = isset($data['type']) ? $data['type'] : null;
            unset($data['type']);
            $value = array_pop($data);
            $terms[] = $this->resolveArgumentValue($types, $value, $valueType);
        }
        return $terms;
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    protected function resolveFilterValues(Variable $subjectVariable, AbstractIri $predicate, array $allowedTypes, array $data): array
    {
        $filters = [];
        $children = array_values($data);
        $child = array_pop($children);
        if (is_array($child)) {
            foreach ($data as $datum) {
                $valueType = isset($datum['type']) ? $datum['type'] : null;
                unset($datum['type']);
                $filterType = key($datum);
                $value = array_pop($datum);
                $term = $this->resolveArgumentValue($allowedTypes, $value, $valueType);
                $filters = array_merge($filters, $this->resolveFilterValue($subjectVariable, $predicate, $term, $filterType));
            }
        }
        else {
            $valueType = isset($data['type']) ? $data['type'] : null;
            unset($data['type']);
            $filterType = key($data);
            $value = array_pop($data);
            $term = $this->resolveArgumentValue($allowedTypes, $value, $valueType);
            $filters = array_merge($filters, $this->resolveFilterValue($subjectVariable, $predicate, $term, $filterType));
        }
        return $filters;
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    protected function resolveFilterValue($subjectVariable, $predicate, TermInterface $term, string $filterType): array
    {
        $filters = [];
        if (
            !in_array($filterType, [Constant::FILTER_EQUAL_TO, Constant::FILTER_NOT_EQUAL_TO, Constant::SEARCH_WILDCARD]) &&
            !($term instanceof AbstractLiteral)
        ) {
            throw new SchemaApiException(sprintf('"%s" is not a literal and cannot be compared using "%s"', $term->serialize(), $filterType));
        }
        // A random string is used to avoid collisions when more than one filter of the same value is applied.
        $filterVariable = new Variable(str_replace('-', '', Uuid::uuid4()->toString()));
        $filters[] = new Triple($subjectVariable, $predicate, $filterVariable);
        switch ($filterType)
        {
            case Constant::FILTER_EQUAL_TO:
                $filters[] = new Filter(new Equal($filterVariable, $term));
                break;

            case Constant::FILTER_GREATER_THAN:
                $filters[] = new Filter(new GreaterThan($filterVariable, $term));
                break;

            case Constant::FILTER_GREATER_THAN_OR_EQUAL_TO:
                $filters[] = new Filter(new GreaterThanOrEqual($filterVariable, $term));
                break;

            case Constant::FILTER_LESS_THAN:
                $filters[] = new Filter(new LessThan($filterVariable, $term));
                break;

            case Constant::FILTER_LESS_THAN_OR_EQUAL_TO:
                $filters[] = new Filter(new LessThanOrEqual($filterVariable, $term));
                break;

            case Constant::FILTER_NOT_EQUAL_TO:
                $filters[] = new Filter(new NotEqual($filterVariable, $term));
                break;

            case Constant::SEARCH_WILDCARD:
                // If term is plain literal, use full-text search.
                if ($term instanceof PlainLiteral) {
                    $bdsTerm = new PrefixedIri('bds', 'search');
                    $filters[] = new Service($bdsTerm, [new Triple($filterVariable, $bdsTerm, $term)]);
                }
                // If term is not plain literal, do not use a full-text search,
                // but include as normal filter.
                else {
                    $filters[] = new Filter(new Equal($filterVariable, $term));
                }
                break;

        }
        return $filters;
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    protected function resolveArgumentValue(array $allowedTypes, string $value, ?string $valueType): TermInterface
    {
        if (
            $valueType !== null &&
            in_array($valueType, $allowedTypes)
        ) {
            // Validate value against type.
            switch ($valueType)
            {
                case 'BooleanBox':
                    $term = new TypedLiteral($value, new PrefixedIri('xsd', 'boolean'));
                    break;

                case 'DateBox':
                    $date = DateTime::createFromFormat('Y-m-d', $value);
                    if ($date === false) {
                        $date = DateTime::createFromFormat('Y-m-dp', $value);
                    }
                    if ($date === false) {
                        $date = DateTime::createFromFormat('Y-m-dP', $value);
                    }
                    if ($date === false) {
                        throw new SchemaApiException(sprintf('Date format of date %s is not valid ISO 8601', $value));
                    }
                    $term = new TypedLiteral($date->format(Constant::XSD_DATE_FORMATS['date']), new PrefixedIri('xsd', 'date'));
                    break;

                case 'DateTimeBox':
                    $date = DateTime::createFromFormat('Y-m-d\TH:i:sP', $value);
                    if ($date === false) {
                        $date = DateTime::createFromFormat('Y-m-d\TH:i:sp', $value);
                    }
                    if ($date === false) {
                        // ISO 8601 Extended Format.
                        $date = DateTime::createFromFormat('Y-m-d\TH:i:s.vP', $value);
                    }
                    if ($date === false) {
                        // ISO 8601 Extended Format.
                        $date = DateTime::createFromFormat('Y-m-d\TH:i:s.vp', $value);
                    }
                    if ($date === false) {
                        throw new SchemaApiException(sprintf('Date format of datetime %s is not valid ISO 8601', $value));
                    }
                    $term = new TypedLiteral($date->format(Constant::XSD_DATE_FORMATS['dateTime']), new PrefixedIri('xsd', 'dateTime'));
                    break;

                case 'FloatBox':
                case 'NumberBox':
                    $term = new TypedLiteral($value, new PrefixedIri('xsd', 'decimal'));
                    break;

                case 'IntegerBox':
                    $term = new TypedLiteral($value, new PrefixedIri('xsd', 'integer'));
                    break;

                case 'TextBox':
                    $term = new PlainLiteral($value);
                    break;

                case 'TimeBox':
                    $date = DateTime::createFromFormat('H:i:s', $value);
                    if ($date === false) {
                        $date = DateTime::createFromFormat('H:i:sP', $value);
                    }
                    if ($date === false) {
                        $date = DateTime::createFromFormat('H:i:sp', $value);
                    }
                    if ($date === false) {
                        throw new SchemaApiException(sprintf('Time format of time %s is not valid ISO 8601', $value));
                    }
                    $term = new TypedLiteral($date->format(Constant::XSD_DATE_FORMATS['time']), new PrefixedIri('xsd', 'time'));
                    break;

                case 'URLBox':
                    $term = $this->attemptCreatePrefixedIri($value);
                    break;

                default:
                    // If it is a role, it must inherit the role type.
                    if (preg_match('/.*Role$/m', $valueType)) {
                        $possibleRoleVariable = new Variable('possibleRole');
                        $statement = $this->sparQlClient
                            ->ask()
                            ->where([
                                new Triple($this->attemptCreatePrefixedIri($value), new PrefixedIri('rdf', 'type'), $possibleRoleVariable),
                                new Triple($possibleRoleVariable, new ZeroOrMorePath(new PrefixedIri('rdfs', 'subClassOf')), new PrefixedIri('schema', 'Role')),
                            ]);
                    }
                    // If it is not a role, it can be any other schema.org class.
                    else {
                        $typeIri = $this->getIri($valueType);
                        $statement = $this->sparQlClient
                            ->ask()
                            ->where([
                                new Triple($this->attemptCreatePrefixedIri($value), new PrefixedIri('rdf', 'type'), $this->attemptCreatePrefixedIri($typeIri)),
                            ]);
                    }
                    // Check if data exists for the given type and value.
                    if ($this->sparQlClient->execute($statement)) {
                        // If it does, the value is a reference to the data.
                        $term = $this->attemptCreatePrefixedIri($value);
                    }
                    // If it doesn't, raise an exception.
                    else {
                        throw new SchemaApiException(sprintf('Value "%s" does not match type %s', $value, $valueType));
                    }
            }
        }
        // For arguments without value, TextBox is assumed as type (if valid).
        elseif (!isset($valueType) && in_array('TextBox', $allowedTypes)) {
            $term = new PlainLiteral($value);
        }
        else {
            throw new SchemaApiException(sprintf('Missing or invalid type for value "%s". Valid types are %s', $value, implode(', ', $allowedTypes)));
        }
        // Ensure that term is valid.
        try {
            $term->serialize();
        }
        catch (SparQlException $exception) {
            throw new SchemaApiException(sprintf('Argument type validation failed: %s', $exception->getMessage()));
        }
        return $term;
    }
}
