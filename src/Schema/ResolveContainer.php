<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SparQlClient\Syntax\Pattern\PatternInterface;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\AbstractIri;
use EffectiveActivism\SparQlClient\Syntax\Term\TermInterface;

class ResolveContainer
{
    protected array $filters;

    protected AbstractIri $incomingProperty;

    protected AbstractIri $property;

    protected TermInterface $value;

    public function hasValue(): bool
    {
        return isset($this->value) && $this->value !== null;
    }

    /**
     * Getters.
     */

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getIncomingProperty(): AbstractIri
    {
        return $this->incomingProperty;
    }

    public function getProperty(): AbstractIri
    {
        return $this->property;
    }

    public function getValue(): TermInterface
    {
        return $this->value;
    }

    /**
     * Setters.
     */

    public function setFilters(array $filters): self
    {
        foreach ($filters as $filter) {
            if (!($filter instanceof PatternInterface)) {
                throw new SchemaApiException('Filter is wrong class');
            }
        }
        $this->filters = $filters;
        return $this;
    }

    public function setIncomingProperty(AbstractIri $incomingProperty): self
    {
        $this->incomingProperty = $incomingProperty;
        return $this;
    }

    public function setProperty(AbstractIri $property): self
    {
         $this->property = $property;
         return $this;
    }

    public function setValue(TermInterface $value): self
    {
        $this->value = $value;
        return $this;
    }
}
