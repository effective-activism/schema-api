<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\Helper;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SparQlClient\Exception\SparQlException;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\AbstractIri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\Iri;
use EffectiveActivism\SparQlClient\Syntax\Term\Iri\PrefixedIri;

trait IriTrait
{
    protected array $iris = [];

    protected array $namespaces = [];

    /**
     * @throws SchemaApiException
     */
    public function getLocalPart(string $iri): string
    {
        $localPart = str_replace(array_values($this->namespaces), '', $iri);
        if ($localPart === $iri) {
            throw new SchemaApiException(sprintf('"%s" has an unknown namespace', $iri));
        }
        return $localPart;
    }

    /**
     * @throws SparQlException
     */
    public function attemptCreatePrefixedIri(string $iri): AbstractIri
    {
        $localPart = str_replace(array_values($this->namespaces), '', $iri);
        if ($localPart !== $iri) {
            foreach ($this->namespaces as $namespacePrefix => $namespaceUrl ) {
                if (str_starts_with($iri, $namespaceUrl)) {
                    return new PrefixedIri($namespacePrefix, $localPart);
                }
            }
        }
        // Conversion failed, return a normal Iri.
        return new Iri($iri);
    }

    /**
     * @throws SparQlException
     */
    public function attemptConvertToPrefixedIri(AbstractIri $term): AbstractIri
    {
        if ($term instanceof Iri) {
            $term = $this->attemptCreatePrefixedIri($term->getRawValue());
        }
        return $term;
    }

    /**
     * @throws SchemaApiException
     * @throws SparQlException
     */
    public function createIriFromPrefixedIri(PrefixedIri $iri): Iri
    {
        foreach ($this->namespaces as $namespacePrefix => $namespaceUrl) {
            if ($iri->getPrefix() === $namespacePrefix) {
                return new Iri(sprintf('%s%s', $namespaceUrl, $iri->getLocalPart()));
            }
        }
        throw new SchemaApiException(sprintf('"%s" has an unknown namespace', $iri->getRawValue()));
    }

    /**
     * @throws SchemaApiException
     */
    public function getIri(string $localPart): string
    {
        if (!isset($this->iris[$localPart])) {
            throw new SchemaApiException(sprintf('"%s" is not found in registry', $localPart));
        }
        return $this->iris[$localPart];
    }
}
