<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\Exception\SchemaApiException;
use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class BooleanBox extends ObjectType
{
    const TRUE = '"true"^^xsd:boolean';

    const FALSE = '"false"^^xsd:boolean';

    public function __construct()
    {
        $config = [
            'name' => 'BooleanBox',
            'description' => 'Boolean: True or False.',
            'fields' => [
                'value' => [
                    'type' => Type::string(),
                    'resolve' => function(ResolveContainer $resolveContainer, array $arguments) {
                        if (!$resolveContainer->hasValue()) {
                            return null;
                        }
                        return match ($resolveContainer->getValue()->serialize()) {
                            self::TRUE => true,
                            self::FALSE => false,
                            default => throw new SchemaApiException(sprintf('"%s" is not a valid boolean', $resolveContainer->getValue()->getRawValue())),
                        };
                    }
                ],
            ],
        ];
        parent::__construct($config);
    }
}
