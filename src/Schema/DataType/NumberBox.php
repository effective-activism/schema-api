<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class NumberBox extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'NumberBox',
            'description' => '<![CDATA[Data type: Number.<br/><br/>Usage guidelines:<br/><br/><ul><li>Use values from 0123456789 (Unicode \'DIGIT ZERO\' (U+0030) to \'DIGIT NINE\' (U+0039)) rather than superficially similiar Unicode symbols.</li><li>Use \'.\' (Unicode \'FULL STOP\' (U+002E)) rather than \',\' to indicate a decimal point. Avoid using these symbols as a readability separator.</li></ul>]]>',
            'fields' => [
                'value' => [
                    'type' => Type::float(),
                    'resolve' => function(ResolveContainer $resolveContainer, array $arguments) {
                        return $resolveContainer->hasValue() ? $resolveContainer->getValue()->getRawValue() : null;
                    }
                ],
            ],
        ];
        parent::__construct($config);
    }
}
