<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class DateBox extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'DateBox',
            'description' => '<![CDATA[A date value in <a href="http://en.wikipedia.org/wiki/ISO_8601">ISO 8601 date format</a>.]]>',
            'fields' => [
                'value' => [
                    'type' => Type::string(),
                    'resolve' => function(ResolveContainer $resolveContainer, array $arguments) {
                        return $resolveContainer->hasValue() ? $resolveContainer->getValue()->getRawValue() : null;
                    }
                ],
            ],
        ];
        parent::__construct($config);
    }
}
