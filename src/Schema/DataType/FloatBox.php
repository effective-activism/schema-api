<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class FloatBox extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'FloatBox',
            'description' => 'Data type: Floating number.',
            'fields' => [
                'value' => [
                    'type' => Type::float(),
                    'resolve' => function(ResolveContainer $resolveContainer, array $arguments) {
                        return $resolveContainer->hasValue() ? $resolveContainer->getValue()->getRawValue() : null;
                    }
                ],
            ],
        ];
        parent::__construct($config);
    }
}
