<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Schema\DataType;

use EffectiveActivism\SchemaApi\Schema\ResolveContainer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class DateTimeBox extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'DateTimeBox',
            'description' => 'A combination of date and time of day in the form [-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm] (see Chapter 5.4 of ISO 8601).',
            'fields' => [
                'value' => [
                    'type' => Type::string(),
                    'resolve' => function(ResolveContainer $resolveContainer, array $arguments) {
                        return $resolveContainer->hasValue() ? $resolveContainer->getValue()->getRawValue() : null;
                    }
                ],
            ],
        ];
        parent::__construct($config);
    }
}
