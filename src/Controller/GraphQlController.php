<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi\Controller;

use EffectiveActivism\SchemaApi\Schema\Registry;
use EffectiveActivism\SparQlClient\Client\SparQlClientInterface;
use GraphQL\Server\StandardServer;
use GraphQL\Type\Schema;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7\Response as Psr7Response;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GraphQlController extends AbstractController
{
    protected Schema $schema;

    protected SparQlClientInterface $sparQlClient;

    protected StandardServer $standardServer;

    public function __construct(Registry $registry, SparQlClientInterface $sparQlClient)
    {
        $this->sparQlClient = $sparQlClient;
        $this->schema = new Schema([
            'mutation' => $registry->getMutation(),
            'query' => $registry->getQuery(),
            'typeLoader' => function($type) use ($registry) {
                // Union types are not part of the schema.org vocabulary but are necessary to reflect the schema.org
                // data model.
                if (str_starts_with($type, 'UnionOf')) {
                    return $registry->getUnion($type);
                }
                else {
                    return $registry->getType($registry->getIri($type));
                }
            }
        ]);
        $this->standardServer = new StandardServer([
            'schema' => $this->schema,
        ]);
    }

    public function api(Request $request): Response
    {
        $psr17Factory = new Psr17Factory();
        $psrHttpFactory = new PsrHttpFactory($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);
        $psrRequest = $psrHttpFactory->createRequest($request);
        if ($psrRequest->getHeader('Content-Type') === ['application/json']) {
            // Standard server expects parsed body to contain the decoded json.
            $data = json_decode($request->getContent(), true);
            $psrRequest = $psrRequest->withParsedBody($data);
        }
        $server = new StandardServer([
            'schema' => $this->schema,
        ]);
        $result = $server->executePsrRequest($psrRequest);
        $psrResponse = new Psr7Response(200, [], json_encode($result));
        $httpFoundationFactory = new HttpFoundationFactory();
        return $httpFoundationFactory->createResponse($psrResponse);
    }
}
