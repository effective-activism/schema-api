<?php declare(strict_types=1);

namespace EffectiveActivism\SchemaApi;

class Constant
{
    const SECURITY_ATTRIBUTE_MUTATION_DELETE = 'schema-api-mutation-delete';

    const SECURITY_ATTRIBUTE_MUTATION_INSERT = 'schema-api-mutation-insert';

    const SECURITY_ATTRIBUTE_MUTATION_UPDATE = 'schema-api-mutation-update';

    const SECURITY_ATTRIBUTE_QUERY_MULTIPLE = 'schema-api-query-multiple';

    const SECURITY_ATTRIBUTE_QUERY_SEARCH = 'schema-api-query-search';

    const SECURITY_ATTRIBUTE_QUERY_SINGULAR = 'schema-api-query-singular';

    const SECURITY_ATTRIBUTE_SUBQUERY = 'schema-api-subquery';

    const BASE_ADDRESS = 'https://schema.org/';

    const BASE_PREFIX = 'schema';

    const PENDING_ADDRESS = 'https://pending.schema.org';

    const GRAPHQL_RESERVED_TYPES = [
        'Int',
        'Float',
        'Boolean',
        'String',
        'ID',
        'null',
        'true',
        'false',
    ];

    const SCHEMA_DATA_TYPES = [
        'Boolean',
        'Date',
        'DateTime',
        'Float',
        'Integer',
        'Number',
        'Text',
        'Time',
        'URL',
    ];

    const XSD_DATE_FORMATS = [
        'date' => 'Y-m-dp',
        'dateTime' => 'Y-m-d\TH:i:s.vp',
        'time' => 'H:i:sp',
    ];

    const FILTER_EQUAL_TO = 'equalTo';
    const FILTER_GREATER_THAN = 'greaterThan';
    const FILTER_GREATER_THAN_OR_EQUAL_TO = 'greaterThanOrEqualTo';
    const FILTER_LESS_THAN = 'lessThan';
    const FILTER_LESS_THAN_OR_EQUAL_TO = 'lessThanOrEqualTo';
    const FILTER_NOT_EQUAL_TO = 'notEqualTo';
    const FILTER_TYPE = 'type';
    const SEARCH_WILDCARD = 'searchFor';
}
